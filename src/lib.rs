extern crate console_error_panic_hook;
extern crate wasm_bindgen;
extern crate web_sys;

use crate::nes::Nes;
use std::fs::File;
use std::ops::Bound::Included;
use wasm_bindgen::prelude::*;
use std::io::Write;
use log::{Level, LevelFilter, Metadata, Record, SetLoggerError};
use std::io::Read;
use std::path::Path;
use wasm_bindgen::JsCast;

mod bus;
mod cartridge;
mod mapper;
mod nes;
mod c2c02;
mod c6502;
mod utils;

struct SimpleLogger;

impl log::Log for SimpleLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= Level::Info
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            println!("{} - {}", record.level(), record.args());
        }
    }

    fn flush(&self) {}
}

static LOGGER: SimpleLogger = SimpleLogger;

pub fn init_logger() -> Result<(), SetLoggerError> {
    log::set_logger(&LOGGER).map(|()| log::set_max_level(LevelFilter::Debug))
}

#[wasm_bindgen]
pub fn init_panic_hook() {
    console_error_panic_hook::set_once();
}

#[wasm_bindgen]
#[derive(Clone, Copy)]
pub struct Registers {
    pub status: u8,
    pub pc: u16,
    pub acc: u8,
    pub x: u8,
    pub y: u8,
    pub stkp: u8,
}

#[wasm_bindgen]
pub struct CpuState {
    pub registers: Registers,
}

static mut _NES: Option<Nes<'static>> = None;
fn get_nes() -> &'static mut Nes<'static> {
    unsafe {
        return _NES.get_or_insert(Nes::new());
    }
}

static mut _CODE: Option<Vec<String>> = None;

#[wasm_bindgen]
pub fn get_code() -> Vec<JsValue> {
    unsafe {
        let nes = get_nes();
        // let code = _CODE.get_or_insert(nes.cpu.disassemble(0x8000, 0xFFFF));

        let mut result: Vec<JsValue> = vec![];
        // for value in code {
        //     result.push(JsValue::from_str(value.as_str()));
        // }

        return result;
    }
}

#[wasm_bindgen]
pub fn reset() {
    let nes = get_nes();
    nes.reset();
}

#[wasm_bindgen]
pub fn controller_input(input: u8) {
    get_nes().controller_input(input);
}

#[wasm_bindgen]
pub fn clock() {
    let nes = get_nes();
    nes.clock();
}

#[wasm_bindgen]
pub fn get_registers() -> Registers {
    let nes = get_nes();
    Registers {
        status: nes.cpu.status,
        pc: nes.cpu.pc,
        acc: nes.cpu.acc,
        x: nes.cpu.x,
        y: nes.cpu.y,
        stkp: nes.cpu.stkp,
    }
}

#[wasm_bindgen]
pub fn read_address(address: u16) {
    let address_value = get_nes().bus.cpu_read(address, true);
    web_sys::console::log_1(&format!("{}", address_value).into());
}

#[wasm_bindgen]
pub fn render_frame(palette_id: u8, button_state: u8) {
    let mut nes = get_nes();
    // nes.controller_input(button_state, true);
    nes.render_frame();
}

#[wasm_bindgen]
pub fn get_screen() -> Vec<u8> {
    get_nes().ppu.get_screen().pixels.clone()
    // let mut res = vec![];

    // let sprite = get_nes().ppu.get_screen();
    // for y in 0..240 {
    //     for x in 0..256 {
    //         res.push(sprite.pixels[y][x].red);
    //         res.push(sprite.pixels[y][x].green);
    //         res.push(sprite.pixels[y][x].blue);
    //         res.push(sprite.pixels[y][x].alpha);
    //     }
    // }

    // res
}

#[wasm_bindgen]
pub fn load_rom(data: Vec<u8>) {
    let nes = get_nes();
    nes.insert_cartridge(data);
    nes.reset();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        // init_logger();

        let nes = get_nes();

        nes.reset();

        // for _ in 0 .. 100 {
            nes.render_frame();
        // }

        // nes.ppu.get_pattern_table(0, 0);

        // for _ in 0 .. 100000 {
            // nes.clock();
        // }

        // load_file();
    }

    fn load_file() {
        let file_name = "roms/nestest.nes";
        let mut f = File::open(file_name).expect("no file found...");
        let metadata = std::fs::metadata(file_name).expect("unable to read metadata");
        let mut buffer = vec![0; metadata.len() as usize];
        f.read(&mut buffer).expect("buffer overflow");
        let path = Path::new("roms/out.txt");
        let display = path.display();
        let mut file = match File::create(path) {
            Err(why) => panic!("couldn't create {}: {}", display, why),
            Ok(file) => file,
        };

        for x in &buffer {
            file.write(format!("{}, ", x).as_bytes());
        }
    }
}
