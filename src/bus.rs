use crate::c6502::C6502;
use crate::c2c02::C2c02;
use crate::cartridge::Cartridge;

pub struct Bus<'a> {
    // device: 2 KB ram
    cpu_ram:          [u8; 2 * 1024],

    // device: cpu
    pub cpu:          Option<*mut C6502<'a>>,

    // device: picture processing unit
    ppu:              Option<*mut C2c02<'a>>,

    // device: cartridge
    pub cartridge:    Option<*mut Cartridge>,
    
    pub controller:   [u8; 2],
    controller_state: [u8; 2],

    pub dma_page:     u8,
    pub dma_addr:     u8,
    pub dma_data:     u8,
    pub dma_transfer: bool,
    pub dma_dummy:    bool
}

impl <'a> Bus<'a> {
    pub fn new() -> Bus<'a> {
        Bus{cpu_ram:          [0x00; 2 * 1024],
            cpu:              None,
            ppu:              None,
            controller:       [0x00; 2],
            controller_state: [0x00; 2],
            cartridge:        None,
            dma_page:         0x00,
            dma_addr:         0x00,
            dma_data:         0x00,
            dma_transfer:     false,
            dma_dummy:        false
        }
    }

    pub fn connect_cpu(&mut self, cpu: &mut C6502<'a>) {
        self.cpu = Some(cpu);
    }

    pub fn connect_ppu(&mut self, ppu: &mut C2c02<'a>) {
        self.ppu = Some(ppu);
    }

    pub fn connect_cartridge(&mut self, cartridge: &mut Cartridge) {
        self.cartridge = Some(cartridge);
    }
}

impl <'a> Bus<'a> {
    pub fn cpu_write(&mut self, addr: u16, data: u8) {
        if addr <= 0x1FFF {
            // the memory is mirrored multiple time in the full address range
            // [0x0000:0x1FFF] has to be mapped into [0x0000:0x07FF] (2k)
            self.cpu_ram[(addr & 0x07FF) as usize] = data;
        } else if (addr >= 0x2000) && (addr <= 0x3FFF) {
            // the memory is mirrored multiple time in the full address range
            // [0x2000:0x3FFF] has to be mapped into [0x0000:0x0007]
            unsafe {
                (*self.ppu.unwrap()).cpu_write(addr & 0x0007, data);
            }
        } else if addr == 0x4014 {
            self.dma_page = data;
            self.dma_addr = 0x00;
            self.dma_transfer = true;
        } else if (addr >= 0x4016) && (addr <= 0x4017) {
            self.controller_state[(addr & 0x0001) as usize] = self.controller[(addr & 0x0001) as usize];
        } else if addr >= 0x4020 {
            // write to cardridge
            unsafe {
                (*self.cartridge.unwrap()).cpu_write(addr, data);
            }
        }
    }

    pub fn cpu_read(&mut self, addr: u16, readOnly: bool) -> u8 {
        let mut data: u8 = 0x00;

        if addr <= 0x1FFF {
            // the memory is mirrored multiple time in the full address range
            // [0x0000:0x1FFF] has to be mapped into [0x0000:0x07FF] (2k)
            data = self.cpu_ram[(addr & 0x07FF) as usize];
        } else if (addr >= 0x2000) && (addr <= 0x3FFF) {
            // the memory is mirrored multiple time in the full address range
            // [0x2000:0x3FFF] has to be mapped into [0x0000:0x0007]
            unsafe {
                data = (*self.ppu.unwrap()).cpu_read(addr & 0x0007, readOnly);
            }
        } else if (addr >= 0x4016) && (addr <= 0x4017) {
            let masked_addr = (addr & 0x0001) as usize;
            data = if (self.controller_state[masked_addr] & 0x80) > 0 {1} else {0};
            self.controller_state[masked_addr] <<= 1;
        } else if addr >= 0x4020 {
            // read from cardridge
            unsafe {
                let read = (*self.cartridge.unwrap()).cpu_read(addr, readOnly);

                if read.is_some() {
                    data = read.unwrap();
                }
            }
        }
        
        return data;
    }
}