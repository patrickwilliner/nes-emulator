use crate::cartridge::Cartridge;
use crate::cartridge::Mirror;
use crate::utils::{Bitset, LoopyReg, Pixel, Sprite};

const COLOR_PALETTE: [Pixel; 0x40] = [
    Pixel{red: 84,  green: 84,  blue: 84,  alpha: 0xFF},
    Pixel{red: 0,   green: 30,  blue: 116, alpha: 0xFF},
    Pixel{red: 8,   green: 16,  blue: 144, alpha: 0xFF},
    Pixel{red: 48,  green: 0,   blue: 136, alpha: 0xFF},
    Pixel{red: 68,  green: 0,   blue: 100, alpha: 0xFF},
    Pixel{red: 92,  green: 0,   blue: 48,  alpha: 0xFF},
    Pixel{red: 84,  green: 4,   blue: 0,   alpha: 0xFF},
    Pixel{red: 60,  green: 24,  blue: 0,   alpha: 0xFF},
    Pixel{red: 32,  green: 42,  blue: 0,   alpha: 0xFF},
    Pixel{red: 8,   green: 58,  blue: 0,   alpha: 0xFF},
    Pixel{red: 0,   green: 64,  blue: 0,   alpha: 0xFF},
    Pixel{red: 0,   green: 60,  blue: 0,   alpha: 0xFF},
    Pixel{red: 0,   green: 50,  blue: 60,  alpha: 0xFF},
    Pixel{red: 0,   green: 0,   blue: 0,   alpha: 0xFF},
    Pixel{red: 0,   green: 0,   blue: 0,   alpha: 0xFF},
    Pixel{red: 0,   green: 0,   blue: 0,   alpha: 0xFF},

    Pixel{red: 152, green: 150, blue: 152, alpha: 0xFF},
    Pixel{red: 8,   green: 76,  blue: 196, alpha: 0xFF},
    Pixel{red: 48,  green: 50,  blue: 236, alpha: 0xFF},
    Pixel{red: 92,  green: 30,  blue: 228, alpha: 0xFF},
    Pixel{red: 136, green: 20,  blue: 176, alpha: 0xFF},
    Pixel{red: 160, green: 20,  blue: 100, alpha: 0xFF},
    Pixel{red: 152, green: 34,  blue: 32,  alpha: 0xFF},
    Pixel{red: 120, green: 60,  blue: 0,   alpha: 0xFF},
    Pixel{red: 84,  green: 90,  blue: 0,   alpha: 0xFF},
    Pixel{red: 40,  green: 114, blue: 0,   alpha: 0xFF},
    Pixel{red: 8,   green: 124, blue: 0,   alpha: 0xFF},
    Pixel{red: 0,   green: 118, blue: 40,  alpha: 0xFF},
    Pixel{red: 0,   green: 102, blue: 120, alpha: 0xFF},
    Pixel{red: 0,   green: 0,   blue: 0,   alpha: 0xFF},
    Pixel{red: 0,   green: 0,   blue: 0,   alpha: 0xFF},
    Pixel{red: 0,   green: 0,   blue: 0,   alpha: 0xFF},

    Pixel{red: 236, green: 238, blue: 236, alpha: 0xFF},
    Pixel{red: 76,  green: 154, blue: 236, alpha: 0xFF},
    Pixel{red: 120, green: 124, blue: 236, alpha: 0xFF},
    Pixel{red: 176, green: 98,  blue: 236, alpha: 0xFF},
    Pixel{red: 228, green: 84,  blue: 236, alpha: 0xFF},
    Pixel{red: 236, green: 88,  blue: 180, alpha: 0xFF},
    Pixel{red: 236, green: 106, blue: 100, alpha: 0xFF},
    Pixel{red: 212, green: 136, blue: 32,  alpha: 0xFF},
    Pixel{red: 160, green: 170, blue: 0,   alpha: 0xFF},
    Pixel{red: 116, green: 196, blue: 0,   alpha: 0xFF},
    Pixel{red: 76,  green: 208, blue: 32,  alpha: 0xFF},
    Pixel{red: 56,  green: 204, blue: 108, alpha: 0xFF},
    Pixel{red: 56,  green: 180, blue: 204, alpha: 0xFF},
    Pixel{red: 60,  green: 60,  blue: 60,  alpha: 0xFF},
    Pixel{red: 0,   green: 0,   blue: 0,   alpha: 0xFF},
    Pixel{red: 0,   green: 0,   blue: 0,   alpha: 0xFF},

    Pixel{red: 236, green: 238, blue: 236, alpha: 0xFF},
    Pixel{red: 168, green: 204, blue: 236, alpha: 0xFF},
    Pixel{red: 188, green: 188, blue: 236, alpha: 0xFF},
    Pixel{red: 212, green: 178, blue: 236, alpha: 0xFF},
    Pixel{red: 236, green: 174, blue: 236, alpha: 0xFF},
    Pixel{red: 236, green: 174, blue: 212, alpha: 0xFF},
    Pixel{red: 236, green: 180, blue: 176, alpha: 0xFF},
    Pixel{red: 228, green: 196, blue: 144, alpha: 0xFF},
    Pixel{red: 204, green: 210, blue: 120, alpha: 0xFF},
    Pixel{red: 180, green: 222, blue: 120, alpha: 0xFF},
    Pixel{red: 168, green: 226, blue: 144, alpha: 0xFF},
    Pixel{red: 152, green: 226, blue: 180, alpha: 0xFF},
    Pixel{red: 160, green: 214, blue: 228, alpha: 0xFF},
    Pixel{red: 160, green: 162, blue: 160, alpha: 0xFF},
    Pixel{red: 0,   green: 0,   blue: 0,   alpha: 0xFF},
    Pixel{red: 0,   green: 0,   blue: 0,   alpha: 0xFF}
];

enum Status {
    Unused0        = 0,
    Unused1        = 1,
    Unused2        = 2,
    Unused3        = 3,
    Unused4        = 4,
    SpriteOverflow = 5,
    SpriteZeroHit  = 6,
    VerticalBlank  = 7
}

enum PpuControl {
    NametableX        = 0,
    NametableY        = 1,
    IncrementMode     = 2,
    PatternSprite     = 3,
    PatternBackground = 4,
    SpriteSize        = 5,
    SlaveMode         = 6, //unused
    EnableNmi         = 7
}

enum Mask {
    Grayscale            = 0,
    RenderBackgroundLeft = 1,
    RenderSpritesLeft    = 2,
    RenderBackground     = 3,
    RenderSprites        = 4,
    EnhanceRed           = 5,
    EnhanceGreen         = 6,
    EnhanceBlue          = 7
}

struct Registers {
    status:          Bitset, // VSO- ----
    mask:            Bitset, // BGRs bMmG
    control:         Bitset, // VPHB SINN

    address_latch:   u8,
    ppu_data_buffer: u8,
    vram_addr:       LoopyReg,
    tram_addr:       LoopyReg,
    fine_x:          u8
}

impl Registers {
    fn new() -> Registers {
        Registers {
            status:          Bitset{ value: 0x00 },
            mask:            Bitset{ value: 0x00 },
            control:         Bitset{ value: 0x00 },

            // indicates whether writing to lo|hi byte 
            address_latch:   0x00,
            // reading from ppu delays 1 clock cycle - this variable buffers the data that is read
            ppu_data_buffer: 0x00,

            vram_addr:       LoopyReg{ value: 0x0000 },
            tram_addr:       LoopyReg{ value: 0x0000 },
            fine_x:          0x00
        }
    }
}

// TODO remove
#[derive(Clone, Copy, Debug)]
pub struct ObjectAttribute {
    y: u8,
    id: u8,
    attribute: u8,
    x: u8
}

impl ObjectAttribute {
    pub fn get(&self, i: u8) -> u8 {
        match i % 4 {
            0 => self.y,
            1 => self.id,
            2 => self.attribute,
            3 => self.x,
            _ => panic!("impossible")
        }
    }

    pub fn set(&mut self, i: u8, value: u8) {
        match i % 4 {
            0 => self.y = value,
            1 => self.id = value,
            2 => self.attribute = value,
            3 => self.x = value,
            _ => panic!("impossible")
        }
    }
}

pub struct C2c02<'a> {
    pub cartridge:              Option<*mut Cartridge>,
    pub frame_complete:         bool,
    pub nmi:                    bool,
    tbl_name:                   [[u8; 1024]; 2],
    tbl_palette:                [u8; 32],
    tbl_pattern:                [[u8; 4096]; 2], // divided into 2 4k pixel planes
    scanline:                   i16,
    cycle:                      i16,
    pal_screen:                 &'a [Pixel; 0x40],
    spr_screen:                 Sprite,
    spr_name_table:             [Sprite; 2],
    spr_pattern_table:          [Sprite; 2],
    registers:                  Registers,

    bg_next_tile_id:            u8,
	bg_next_tile_attrib:        u8,
	bg_next_tile_lsb:           u8,
	bg_next_tile_msb:           u8,
	bg_shifter_pattern_lo:      u16,
	bg_shifter_pattern_hi:      u16,
	bg_shifter_attrib_lo:       u16,
    bg_shifter_attrib_hi:       u16,
    
    pub oam:                    [ObjectAttribute; 64],
    oam_address:                u8,
    sprite_scanline:            [ObjectAttribute; 8],
    sprite_count:               u8,
    sprite_shifter_pattern_lo:  [u8; 8],
    sprite_shifter_pattern_hi:  [u8; 8],
    sprite_zero_hit_possible:   bool,
	sprite_zero_being_rendered: bool
}

impl <'a> C2c02<'a> {
    pub fn new() -> C2c02<'a> {
        C2c02 {
            cartridge:                  None,
            tbl_name:                   [[0x00; 1024]; 2],
            tbl_palette:                [0x00; 32],
            tbl_pattern:                [[0x00; 4096]; 2],
            frame_complete:             false,
            scanline:                   0,
            cycle:                      0,
            pal_screen:                 &COLOR_PALETTE,
            spr_screen:                 Sprite::new(340, 261),
            spr_name_table:             [Sprite::new(256, 240), Sprite::new(256, 240)],
            spr_pattern_table:          [Sprite::new(128, 128), Sprite::new(128, 128)],
            registers:                  Registers::new(),
            nmi:                        false,

            bg_next_tile_id:            0x00,
	        bg_next_tile_attrib:        0x00,
	        bg_next_tile_lsb:           0x00,
	        bg_next_tile_msb:           0x00,
	        bg_shifter_pattern_lo:      0x0000,
	        bg_shifter_pattern_hi:      0x0000,
	        bg_shifter_attrib_lo:       0x0000,
            bg_shifter_attrib_hi:       0x0000,
            
            oam:                        [ObjectAttribute{y: 0x00, id: 0x00, attribute: 0x00, x: 0x00}; 64],
            oam_address:                0x00,
            sprite_scanline:            [ObjectAttribute{y: 0x00, id: 0x00, attribute: 0x00, x: 0x00}; 8],
            sprite_count:               0,
            sprite_shifter_pattern_lo:  [0x00; 8],
            sprite_shifter_pattern_hi:  [0x00; 8],
            sprite_zero_hit_possible:   false,
            sprite_zero_being_rendered: false
        }
    }
}

impl <'a> C2c02<'a> {
    pub fn get_screen(&self) -> &Sprite {
        &self.spr_screen
    }

    fn get_name_table(&self, idx: usize) -> &Sprite {
        &self.spr_name_table[idx]
    }

    pub fn get_pattern_table(&mut self, idx: usize, palette_idx: u8) -> &Sprite {
        // tile size = 16 x 16 sprites
        for tile_y in 0..16 {
            for tile_x in 0..16 {
                let offset = tile_y * 256 + tile_x * 16;

                // every strite has 8 x 8 pixels
                for row in 0..8 {
                    // 0x1000 = 4kB pattern size -> full pattern table size = 2 tables * 4kB
                    let mut tile_lsb: u8 = self.ppu_read((idx * 0x1000 + offset + row + 0x0000) as u16, true);
                    let mut tile_msb: u8 = self.ppu_read((idx * 0x1000 + offset + row + 0x0008) as u16, true);
                    
                    for col in 0..8 {
                        // pixel = 0|1|2|3
                        let pixel: u8 = ((tile_lsb & 0x01) << 1) | (tile_msb & 0x01);

                        tile_lsb >>= 1;
                        tile_msb >>= 1;

                        let x: i16 = (tile_x * 8 + (7 - col)) as i16;
                        let y: i16 = (tile_y * 8 + row) as i16;
                        let color = self.get_color_from_palette_ram(palette_idx as u8, pixel);

                        self.spr_pattern_table[idx].set_pixel(x, y, color);
                    }
                }
            }
        }

        &self.spr_pattern_table[idx]
    }

    fn get_color_from_palette_ram(&self, palette: u8, pixel: u8) -> &'a Pixel {
        // palette starting address + (4 * palette_id) + pixel
        let pixel_addr = 0x3F00 + ((palette as u16) << 2) + (pixel as u16);
        let color_palette_id = self.ppu_read(pixel_addr, true) & 0x3F;
        &self.pal_screen[color_palette_id as usize]
    }
}

impl <'a> C2c02<'a> {
    pub fn cpu_read(&mut self, addr: u16, read_only: bool) -> u8 {
        let mut temp: u8 = 0x00;

        match addr {
            0x0000 => (), // Control
            0x0001 => (), // Mask
            0x0002 => { // Status
                temp = self.registers.status.value & 0xE0 | (self.registers.ppu_data_buffer & 0x1F);
                self.registers.status.unset(Status::VerticalBlank as u8);
                self.registers.address_latch = 0;
            }, 
            0x0003 => (), // OAM Address
            0x0004 => { // OAM Data
                temp = self.oam[(self.oam_address / 4) as usize].get(self.oam_address);
            },
            0x0005 => (), // Scroll
            0x0006 => (), // PPU Address
            0x0007 => { // PPU Data
                // delays 1 clock cycle
                temp = self.registers.ppu_data_buffer;
                self.registers.ppu_data_buffer = self.ppu_read(self.registers.vram_addr.value, true);

                // for reading the palettes the 1 cycle delay does not exist
                // this is a hardware thing 
                if self.registers.vram_addr.value >= 0x3F00 {
                    temp = self.registers.ppu_data_buffer;
                }

                self.auto_increment_ppu_address();
            }, 
            _ => {}
        }

        temp
    }

    pub fn cpu_write(&mut self, addr: u16, data: u8) {
        match addr {
            0x0000 => { // Control
                self.registers.control.value = data;
                self.registers.tram_addr.set_nametable_x(self.registers.control.get(PpuControl::NametableX as u8) as u16);
                self.registers.tram_addr.set_nametable_y(self.registers.control.get(PpuControl::NametableY as u8) as u16);
            }, 
            0x0001 => { // Mask
                self.registers.mask.value = data;
            }, 
            0x0002 => (), // Status
            0x0003 => { // OAM Address
                self.oam_address = data;
            },
            0x0004 => { // OAM Datas
                self.oam[(self.oam_address / 4) as usize].set(self.oam_address, data);
            },
            0x0005 => { // Scroll
                if self.registers.address_latch == 0 {
                    self.registers.fine_x = data & 0x07;
                    self.registers.tram_addr.set_coarse_x((data >> 3) as u16);
                    self.registers.address_latch = 1;
                } else {
                    self.registers.tram_addr.set_fine_y((data & 0x07) as u16);
                    self.registers.tram_addr.set_coarse_y((data >> 3) as u16);
                    self.registers.address_latch = 0;
                }
            },
            0x0006 => { // PPU Address
                if self.registers.address_latch == 0 {
                    // store lo byte
                    self.registers.tram_addr.value = (((data & 0x3F) as u16) << 8) | (self.registers.tram_addr.value & 0x00FF);
                    self.registers.address_latch = 1; 
                } else {
                    // store hi byte
                    self.registers.tram_addr.value = (self.registers.tram_addr.value & 0xFF00) | data as u16;
                    self.registers.vram_addr.value = self.registers.tram_addr.value;
                    self.registers.address_latch = 0;
                }
            }, 
            0x0007 => { // PPU Data
                self.ppu_write(self.registers.vram_addr.value, data);
                self.auto_increment_ppu_address();
            }, 
            _ => {}
        }
    }

    fn get_cartridge(&self) -> &Cartridge {
        unsafe {
            return &*self.cartridge.unwrap();
        }
    }

    pub fn ppu_read(&self, mut addr: u16, read_only: bool) -> u8 {
        let mut data: u8 = 0x00;
        addr &= 0x3FFF;
        let cartridge_read = self.get_cartridge().ppu_read(addr, true);

        if cartridge_read.is_some() {
            data = cartridge_read.unwrap();
        } else if (addr >= 0x0000) && (addr <= 0x1FFF) {
            // msb defines left or right pattern table - the rest of the address defines 
            let palette = (addr & 0x1000) >> (3 * 4);
            data = self.tbl_pattern[palette as usize][(addr & 0x0FFF) as usize];
        } else if (addr >= 0x2000) && (addr <= 0x3EFF) {
            addr &= 0x0FFF;

            match self.get_cartridge().mirror {
                Mirror::Vertical => {
                    if (addr >= 0x0000) && (addr <= 0x03FF) {
                        data = self.tbl_name[0][(addr & 0x03FF) as usize];
                    } else if (addr >= 0x0400) && (addr <= 0x07FF) {
                        data = self.tbl_name[1][(addr & 0x03FF) as usize];
                    } else if (addr >= 0x0800) && (addr <= 0x0BFF) {
                        data = self.tbl_name[0][(addr & 0x03FF) as usize];
                    } else if (addr >= 0x0C00) && (addr <= 0x0FFF) {
                        data = self.tbl_name[1][(addr & 0x03FF) as usize];
                    }
                },
                Mirror::Horizontal => {
                    if (addr >= 0x0000) && (addr <= 0x03FF) {
                        data = self.tbl_name[0][(addr & 0x03FF) as usize];
                    } else if (addr >= 0x0400) && (addr <= 0x07FF) {
                        data = self.tbl_name[0][(addr & 0x03FF) as usize];
                    } else if (addr >= 0x0800) && (addr <= 0x0BFF) {
                        data = self.tbl_name[1][(addr & 0x03FF) as usize];
                    } else if (addr >= 0x0C00) && (addr <= 0x0FFF) {
                        data = self.tbl_name[1][(addr & 0x03FF) as usize];
                    }
                },
                _ => {
                }
            }
        } else if (addr >= 0x3F00) && (addr <= 0x3FFF) {
            // palette

            addr &= 0x001F;
            if addr == 0x0010 {
                addr = 0x0000;
            } else if addr == 0x0014 {
                addr = 0x0004;
            } else if addr == 0x0018 {
                addr = 0x0008;
            } if addr == 0x001C {
                addr = 0x000C;
            }
            data = self.tbl_palette[addr as usize] & (if self.registers.mask.get(Mask::Grayscale as u8) == 1 {0x30} else {0x3F});
        }

        data
    }
    
    pub fn ppu_write(&mut self, mut addr: u16, data: u8) {
        addr &= 0x3FFF;
        
        let cartridge_write = self.cartridge.as_mut()
        .map(|cartridge| unsafe {
            return (**cartridge).ppu_write(addr, data);
        });

        if cartridge_write.is_none() || !cartridge_write.unwrap() {
            // normally it's a rom - but on some cartridges it is a ram

            if (addr >= 0x0000) && (addr <= 0x1FFF) {
                // msb defines left or right pattern table - the rest of the address defines 
                let palette = (addr & 0x1000) >> (3 * 4);
                self.tbl_pattern[palette as usize][(addr & 0x0FFF) as usize] = data;
            } else if (addr >= 0x2000) && (addr <= 0x3EFF) {
                addr &= 0x0FFF;

                match self.get_cartridge().mirror {
                    Mirror::Vertical => {
                        if (addr >= 0x0000) && (addr <= 0x03FF) {
                            self.tbl_name[0][(addr & 0x03FF) as usize] = data;
                        } else if (addr >= 0x0400) && (addr <= 0x07FF) {
                            self.tbl_name[1][(addr & 0x03FF) as usize] = data;
                        } else if (addr >= 0x0800) && (addr <= 0x0BFF) {
                            self.tbl_name[0][(addr & 0x03FF) as usize] = data;
                        } else if (addr >= 0x0C00) && (addr <= 0x0FFF) {
                            self.tbl_name[1][(addr & 0x03FF) as usize] = data;
                        }
                    },
                    Mirror::Horizontal => {
                        if (addr >= 0x0000) && (addr <= 0x03FF) {
                            self.tbl_name[0][(addr & 0x03FF) as usize] = data;
                        } else if (addr >= 0x0400) && (addr <= 0x07FF) {
                            self.tbl_name[0][(addr & 0x03FF) as usize] = data;
                        } else if (addr >= 0x0800) && (addr <= 0x0BFF) {
                            self.tbl_name[1][(addr & 0x03FF) as usize] = data;
                        } else if (addr >= 0x0C00) && (addr <= 0x0FFF) {
                            self.tbl_name[1][(addr & 0x03FF) as usize] = data;
                        }
                    },
                    _ => {
                    }
                }
            } else if (addr >= 0x3F00) && (addr <= 0x3FFF) {
                addr &= 0x001F;
                if addr == 0x0010 {
                    addr = 0x0000;
                } else if addr == 0x0014 {
                    addr = 0x0004;
                } else if addr == 0x0018 {
                    addr = 0x0008;
                } if addr == 0x001C {
                    addr = 0x000C;
                }
                self.tbl_palette[addr as usize] = data;
            }
        }
    }

    fn auto_increment_ppu_address(&mut self) {
        if self.registers.control.get(PpuControl::IncrementMode as u8) == 1 {
            self.registers.vram_addr.value = self.registers.vram_addr.value + 32;
        } else {
            self.registers.vram_addr.value = self.registers.vram_addr.value + 1;
        }
    }
}

impl <'a> C2c02<'a> {
    pub fn connect_cartridge(&mut self, cartridge: &mut Cartridge) {
        self.cartridge = Some(cartridge);
    }

    pub fn clock(&mut self) {
        if (self.scanline >= -1) && (self.scanline < 240) {
            if (self.scanline == 0) && (self.cycle == 0) {
                self.cycle = 1;
            }

            if (self.scanline == -1) && (self.cycle == 1) {
                // Effectively start of new frame, so clear vertical blank flag
                self.registers.status.unset(Status::VerticalBlank as u8);
                
                self.registers.status.unset(Status::SpriteOverflow as u8);
                
                // Clear the sprite zero hit flag
                self.registers.status.unset(Status::SpriteZeroHit as u8);

                // Clear Shifters
                for i in 0..8 {
                    self.sprite_shifter_pattern_lo[i] = 0;
                    self.sprite_shifter_pattern_hi[i] = 0;
                }
            }

            if ((self.cycle >= 2) && (self.cycle < 258)) || ((self.cycle >= 321) && (self.cycle < 338)) {
                self.update_shifters();

                match (self.cycle - 1) % 8 {
                    0 => {
                        self.load_background_shifters();
                        self.bg_next_tile_id = self.ppu_read(0x2000 | (self.registers.vram_addr.value & 0x0FFF), true);
                    },
                    2 => {
                        self.bg_next_tile_attrib = self.ppu_read(
                            0x23C0
                            | (self.registers.vram_addr.get_nametable_y() << 11)
                            | (self.registers.vram_addr.get_nametable_x() << 10)
                            | ((self.registers.vram_addr.get_coarse_y() >> 2) << 3)
                            | (self.registers.vram_addr.get_coarse_x() >> 2),
                            true
                        );

                        if (self.registers.vram_addr.get_coarse_y() & 0x02) != 0 {
                            self.bg_next_tile_attrib >>= 4;
                        }

                        if (self.registers.vram_addr.get_coarse_x() & 0x02) != 0 {
                            self.bg_next_tile_attrib >>= 2;
                        }

				        self.bg_next_tile_attrib &= 0x03;
                    },
                    4 => {
                        self.bg_next_tile_lsb = self.ppu_read(
                            ((self.registers.control.get(PpuControl::PatternBackground as u8) as u16) << 12)
                            + ((self.bg_next_tile_id as u16) << 4)
                            + self.registers.vram_addr.get_fine_y()
                            + 0,
                            true
                        );
                    },
                    6 => {
                        self.bg_next_tile_msb = self.ppu_read(
                            ((self.registers.control.get(PpuControl::PatternBackground as u8) as u16) << 12)
					        + ((self.bg_next_tile_id as u16) << 4)
                            + self.registers.vram_addr.get_fine_y()
                            + 8,
                            true
                        );
                    },
                    7 => {
                        self.increment_scroll_x();
                    },
                    _ => {}
                }
            }

            // End of a visible scanline, so increment downwards...
            if self.cycle == 256 {
                self.increment_scroll_y();
            }

            //...and reset the x position
            if self.cycle == 257 {
                self.load_background_shifters();
                self.transfer_address_x();
            }

            // Superfluous reads of tile id at end of scanline
            if (self.cycle == 338) || (self.cycle == 340) {
                self.bg_next_tile_id = self.ppu_read(0x2000 | (self.registers.vram_addr.value & 0x0FFF), true);
            }

            if (self.scanline == -1) && (self.cycle >= 280) && (self.cycle < 305) {
                // End of vertical blank period so reset the Y address ready for rendering
                self.transfer_address_y();
            }

            self.render_foreground();
        }

        if self.scanline == 240 {
		    // Post Render Scanline - Do Nothing!
        }
        
        if (self.scanline >= 241) && (self.scanline < 261) {
            if (self.scanline == 241) && (self.cycle == 1) {
                // Effectively end of frame, so set vertical blank flag
                self.registers.status.set(Status::VerticalBlank as u8);

                // If the control register tells us to emit a NMI when
                // entering vertical blanking period, do it! The CPU
                // will be informed that rendering is complete so it can
                // perform operations with the PPU knowing it wont
                // produce visible artefacts
                if self.registers.control.get(PpuControl::EnableNmi as u8) == 1 {
                    self.nmi = true;
                }
            }
        }

        let mut bg_pixel: u8 = 0x00;   // The 2-bit pixel to be rendered
        let mut bg_palette: u8 = 0x00; // The 3-bit index of the palette the pixel indexes

        if self.registers.mask.get(Mask::RenderBackground as u8) == 1 {
            let bit_mux: u16 = 0x8000 >> self.registers.fine_x;

            let p0_pixel: u8 = if (self.bg_shifter_pattern_lo & bit_mux) > 0 {1} else {0};
            let p1_pixel: u8 = if (self.bg_shifter_pattern_hi & bit_mux) > 0 {1} else {0};
            bg_pixel = (p1_pixel << 1) | p0_pixel;

            let bg_pal0: u8 = if (self.bg_shifter_attrib_lo & bit_mux) > 0 {1} else {0};
            let bg_pal1: u8 = if (self.bg_shifter_attrib_hi & bit_mux) > 0 {1} else {0};
            bg_palette = (bg_pal1 << 1) | bg_pal0;
        }

        let pixel_palette_tupel = self.render_sprites(bg_pixel, bg_palette);

        self.spr_screen.set_pixel(self.cycle - 1, self.scanline, self.get_color_from_palette_ram(pixel_palette_tupel.1, pixel_palette_tupel.0));

        self.cycle += 1;
    
        if self.cycle >= 341 {
            self.cycle = 0;
            self.scanline += 1;

            if self.scanline >= 261 {
                self.scanline = -1;
                self.frame_complete = true;
            }
	    }
    }
}

// clock helpers
impl <'a> C2c02<'a> {
    fn is_rendering_enabled(&self) -> bool {
        (self.registers.mask.get(Mask::RenderBackground as u8) == 1) || (self.registers.mask.get(Mask::RenderSprites as u8) == 1)
    }

    fn increment_scroll_x(&mut self) {
        if self.is_rendering_enabled() {
            if self.registers.vram_addr.get_coarse_x() == 31 {
				// Leaving nametable so wrap address round
				self.registers.vram_addr.set_coarse_x(0);
				// Flip target nametable bit
				self.registers.vram_addr.set_nametable_x(!self.registers.vram_addr.get_nametable_x());
			} else {
                // Staying in current nametable, so just increment
                self.registers.vram_addr.set_coarse_x(self.registers.vram_addr.get_coarse_x() + 1);
			}
        }
    }

    fn increment_scroll_y(&mut self) {
        if self.is_rendering_enabled() {
            if self.registers.vram_addr.get_fine_y() < 7 {
				self.registers.vram_addr.set_fine_y(self.registers.vram_addr.get_fine_y() + 1);
			} else {
                self.registers.vram_addr.set_fine_y(0);

                if self.registers.vram_addr.get_coarse_y() == 29 {
					// We do, so reset coarse y offset
					self.registers.vram_addr.set_coarse_y(0);
					// And flip the target nametable bit
					self.registers.vram_addr.set_nametable_y(!self.registers.vram_addr.get_nametable_y());
				} else if self.registers.vram_addr.get_coarse_y() == 31 {
					// In case the pointer is in the attribute memory, we
					// just wrap around the current nametable
					self.registers.vram_addr.set_coarse_y(0);
				} else {
					// None of the above boundary/wrapping conditions apply
					// so just increment the coarse y offset
					self.registers.vram_addr.set_coarse_y(self.registers.vram_addr.get_coarse_y() + 1);
				}
            }
        }
    }

    fn transfer_address_x(&mut self) {
		if self.is_rendering_enabled() {
            self.registers.vram_addr.set_nametable_x(self.registers.tram_addr.get_nametable_x());
            self.registers.vram_addr.set_coarse_x(self.registers.tram_addr.get_coarse_x());
		}
    }

    fn transfer_address_y(&mut self) {
		if self.is_rendering_enabled() {
            self.registers.vram_addr.set_fine_y(self.registers.tram_addr.get_fine_y());
            self.registers.vram_addr.set_nametable_y(self.registers.tram_addr.get_nametable_y());
            self.registers.vram_addr.set_coarse_y(self.registers.tram_addr.get_coarse_y());
        }
    }

    fn load_background_shifters(&mut self) {
        self.bg_shifter_pattern_lo = (self.bg_shifter_pattern_lo & 0xFF00) | (self.bg_next_tile_lsb as u16);
		self.bg_shifter_pattern_hi = (self.bg_shifter_pattern_hi & 0xFF00) | (self.bg_next_tile_msb as u16);
		self.bg_shifter_attrib_lo = (self.bg_shifter_attrib_lo & 0xFF00) | (if (self.bg_next_tile_attrib & 0b01) != 0 {0xFF} else {0x00});
        self.bg_shifter_attrib_hi = (self.bg_shifter_attrib_hi & 0xFF00) | (if (self.bg_next_tile_attrib & 0b10) != 0 {0xFF} else {0x00});
    }

    fn update_shifters(&mut self) {
        if self.registers.mask.get(Mask::RenderBackground as u8) == 1 {
			// Shifting background tile pattern row
			self.bg_shifter_pattern_lo <<= 1;
			self.bg_shifter_pattern_hi <<= 1;

			// Shifting palette attributes by 1
			self.bg_shifter_attrib_lo <<= 1;
			self.bg_shifter_attrib_hi <<= 1;
        }
        
        if (self.registers.mask.get(Mask::RenderSprites as u8) == 1) && (self.cycle >= 1) && (self.cycle < 258) {
			for i in 0..(self.sprite_count as usize) {
				if self.sprite_scanline[i].x > 0 {
					self.sprite_scanline[i].x -= 1;
				} else {
					self.sprite_shifter_pattern_lo[i] <<= 1;
					self.sprite_shifter_pattern_hi[i] <<= 1;
				}
			}
		}
    }

    // this part is not cycle accurate - might be source of some game incompatibilities
    fn render_foreground(&mut self) {
        if (self.cycle == 257) && (self.scanline >= 0) {
            // reset sprite
            for i in 0..8 {
                self.sprite_scanline[i].y = 0xFF;
                self.sprite_scanline[i].id = 0xFF;
                self.sprite_scanline[i].attribute = 0xFF;
                self.sprite_scanline[i].x = 0xFF;

                self.sprite_shifter_pattern_lo[i] = 0;
                self.sprite_shifter_pattern_hi[i] = 0;
            }
            self.sprite_count = 0;

            let mut oam_entry: u8 = 0;

            self.sprite_zero_hit_possible = false;

            while (oam_entry < 64) && (self.sprite_count < 9) {
                let diff: i16 = (self.scanline as i16) - (self.oam[oam_entry as usize].y as i16);

                if (diff >= 0) && (diff < (if self.registers.control.get(PpuControl::SpriteSize as u8) == 1 {16} else {8})) {
                    if self.sprite_count < 8 {
						// Is this sprite sprite zero?
						if oam_entry == 0 {
							// It is, so its possible it may trigger a 
							// sprite zero hit when drawn
							self.sprite_zero_hit_possible = true;
						}

                        self.sprite_scanline[self.sprite_count as usize] = self.oam[oam_entry as usize].clone();
						self.sprite_count += 1;
                    }
                }

                oam_entry += 1;
            }

            self.registers.status.set_bool(Status::SpriteOverflow as u8, self.sprite_count > 8);
        }

        if self.cycle == 340 {
            for i in 0..(self.sprite_count as usize) {
                let mut sprite_pattern_bits_lo: u8 = 0x00;
                let mut sprite_pattern_bits_hi: u8 = 0x00;
                let mut sprite_pattern_addr_lo: u16 = 0x0000;
                let mut sprite_pattern_addr_hi: u16 = 0x0000;
                
                if self.registers.control.get(PpuControl::SpriteSize as u8) == 0 {
                    // 8x8 Sprite Mode - The control register determines the pattern table
                    if (self.sprite_scanline[i].attribute & 0x80) == 0 {
                        // Sprite is NOT flipped vertically, i.e. normal    
						sprite_pattern_addr_lo = 
                        ((self.registers.control.get(PpuControl::PatternSprite as u8) as u16) << 12)  // Which Pattern Table? 0KB or 4KB offset
                      | ((self.sprite_scanline[i].id as u16) << 4)  // Which Cell? Tile ID * 16 (16 bytes per tile)
                      | ((self.scanline - (self.sprite_scanline[i].y as i16)) as u16); // Which Row in cell? (0->7)
                    } else {
                        // Sprite is flipped vertically, i.e. upside down
						sprite_pattern_addr_lo = 
                        ((self.registers.control.get(PpuControl::PatternSprite as u8) as u16) << 12)  // Which Pattern Table? 0KB or 4KB offset
                      | ((self.sprite_scanline[i].id as u16) << 4)  // Which Cell? Tile ID * 16 (16 bytes per tile)
                      | ((7 as u16) - ((self.scanline - (self.sprite_scanline[i].y as i16)) as u16)); // Which Row in cell? (7->0)
                    }
                } else {
                    // 8x16 Sprite Mode - The sprite attribute determines the pattern table
					if (self.sprite_scanline[i].attribute & 0x80) == 0 {
						// Sprite is NOT flipped vertically, i.e. normal
						if (self.scanline - (self.sprite_scanline[i].y as i16)) < 8 {
							// Reading Top half Tile
							sprite_pattern_addr_lo = 
							  (((self.sprite_scanline[i].id & 0x01) as u16) << 12)  // Which Pattern Table? 0KB or 4KB offset
							| (((self.sprite_scanline[i].id & 0xFE) as u16) << 4)  // Which Cell? Tile ID * 16 (16 bytes per tile)
							| (((self.scanline - (self.sprite_scanline[i].y as i16)) & 0x07) as u16); // Which Row in cell? (0->7)
						} else {
							// Reading Bottom Half Tile
							sprite_pattern_addr_lo = 
							  (((self.sprite_scanline[i].id & 0x01) as u16) << 12)  // Which Pattern Table? 0KB or 4KB offset
							| (((self.sprite_scanline[i].id & 0xFE) as u16 + 1) << 4)  // Which Cell? Tile ID * 16 (16 bytes per tile)
							| ((((self.scanline - (self.sprite_scanline[i].y) as i16)) & 0x07) as u16); // Which Row in cell? (0->7)
						}
					} else {
						// Sprite is flipped vertically, i.e. upside down
						if (self.scanline - (self.sprite_scanline[i].y as i16)) < 8 {
							// Reading Top half Tile
							sprite_pattern_addr_lo = 
							  (((self.sprite_scanline[i].id & 0x01) as u16) << 12)    // Which Pattern Table? 0KB or 4KB offset
							| (((self.sprite_scanline[i].id & 0xFE) as u16 + 1) << 4)    // Which Cell? Tile ID * 16 (16 bytes per tile)
							| (((7 - (self.scanline - self.sprite_scanline[i].y as i16)) & 0x07) as u16); // Which Row in cell? (0->7)
						} else {
							// Reading Bottom Half Tile
							sprite_pattern_addr_lo = 
							  (((self.sprite_scanline[i].id & 0x01) as u16) << 12)    // Which Pattern Table? 0KB or 4KB offset
							| (((self.sprite_scanline[i].id & 0xFE) as u16) << 4)    // Which Cell? Tile ID * 16 (16 bytes per tile)
							| (((7 - (self.scanline - self.sprite_scanline[i].y as i16)) & 0x07) as u16); // Which Row in cell? (0->7)
						}
					}
                }

                // Hi bit plane equivalent is always offset by 8 bytes from lo bit plane
				sprite_pattern_addr_hi = sprite_pattern_addr_lo + 8;

				// Now we have the address of the sprite patterns, we can read them
				sprite_pattern_bits_lo = self.ppu_read(sprite_pattern_addr_lo, true);
				sprite_pattern_bits_hi = self.ppu_read(sprite_pattern_addr_hi, true);

				// If the sprite is flipped horizontally, we need to flip the 
				// pattern bytes. 
				if (self.sprite_scanline[i].attribute & 0x40) != 0 {
					// Flip Patterns Horizontally
					sprite_pattern_bits_lo = self.flipbyte(sprite_pattern_bits_lo);
					sprite_pattern_bits_hi = self.flipbyte(sprite_pattern_bits_hi);
				}

				// Finally! We can load the pattern into our sprite shift registers
				// ready for rendering on the next scanline
				self.sprite_shifter_pattern_lo[i] = sprite_pattern_bits_lo;
				self.sprite_shifter_pattern_hi[i] = sprite_pattern_bits_hi;
            }
        }
    }

    fn render_sprites(&mut self, bg_pixel: u8, bg_palette: u8) -> (u8, u8) {
        let mut fg_pixel: u8 = 0x00;   // The 2-bit pixel to be rendered
        let mut fg_palette: u8 = 0x00; // The 3-bit index of the palette the pixel indexes
        let mut fg_priority: u8 = 0x00;// A bit of the sprite attribute indicates if its
                                // more important than the background
        if self.registers.mask.get(Mask::RenderSprites as u8) == 1 {
            // Iterate through all sprites for this scanline. This is to maintain
            // sprite priority. As soon as we find a non transparent pixel of
            // a sprite we can abort

            self.sprite_zero_being_rendered = false;

            for i in 0..(self.sprite_count as usize) {
                // Scanline cycle has "collided" with sprite, shifters taking over
                if self.sprite_scanline[i].x == 0 {
                    // Note Fine X scrolling does not apply to sprites, the game
                    // should maintain their relationship with the background. So
                    // we'll just use the MSB of the shifter
                    
                    // Determine the pixel value...
                    let fg_pixel_lo: u8 = if (self.sprite_shifter_pattern_lo[i] & 0x80) > 0 {1} else {0};
                    let fg_pixel_hi: u8 = if (self.sprite_shifter_pattern_hi[i] & 0x80) > 0 {1} else {0};
                    fg_pixel = (fg_pixel_hi << 1) | fg_pixel_lo;

                    // Extract the palette from the bottom two bits. Recall
                    // that foreground palettes are the latter 4 in the 
                    // palette memory.
                    fg_palette = (self.sprite_scanline[i].attribute & 0x03) + 0x04;
                    fg_priority = if (self.sprite_scanline[i].attribute & 0x20) == 0 {1} else {0};

                    // If pixel is not transparent, we render it, and dont
                    // bother checking the rest because the earlier sprites
                    // in the list are higher priority
                    if fg_pixel != 0 {
                        if i == 0 { // Is this sprite zero?
                            self.sprite_zero_being_rendered = true;
                        }

                        break;
                    }				
                }
            }		
        }

        // Now we have a background pixel and a foreground pixel. They need
        // to be combined. It is possible for sprites to go behind background
        // tiles that are not "transparent", yet another neat trick of the PPU
        // that adds complexity for us poor emulator developers...

        let mut pixel: u8 = 0x00;   // The FINAL Pixel...
        let mut palette: u8 = 0x00; // The FINAL Palette...

        if (bg_pixel == 0) && (fg_pixel == 0) {
            // The background pixel is transparent
            // The foreground pixel is transparent
            // No winner, draw "background" colour
            pixel = 0x00;
            palette = 0x00;
        } else if (bg_pixel == 0) && (fg_pixel > 0) {
            // The background pixel is transparent
            // The foreground pixel is visible
            // Foreground wins!
            pixel = fg_pixel;
            palette = fg_palette;
        } else if (bg_pixel > 0) && (fg_pixel == 0) {
            // The background pixel is visible
            // The foreground pixel is transparent
            // Background wins!
            pixel = bg_pixel;
            palette = bg_palette;
        } else if (bg_pixel > 0) && (fg_pixel > 0) {
            // The background pixel is visible
            // The foreground pixel is visible
            // Hmmm...
            if fg_priority != 0 {
                // Foreground cheats its way to victory!
                pixel = fg_pixel;
                palette = fg_palette;
            } else {
                // Background is considered more important!
                pixel = bg_pixel;
                palette = bg_palette;
            }

            // Sprite Zero Hit detection
            if self.sprite_zero_hit_possible && self.sprite_zero_being_rendered {
                // Sprite zero is a collision between foreground and background
                // so they must both be enabled
                if ((self.registers.mask.get(Mask::RenderBackground as u8) & self.registers.mask.get(Mask::RenderSprites as u8))) != 0 {
                    // The left edge of the screen has specific switches to control
                    // its appearance. This is used to smooth inconsistencies when
                    // scrolling (since sprites x coord must be >= 0)
                    if (!(self.registers.mask.get(Mask::RenderBackgroundLeft as u8) | self.registers.mask.get(Mask::RenderSpritesLeft as u8))) != 0 {
                        if (self.cycle >= 9) && (self.cycle < 258) {
                            self.registers.status.set(Status::SpriteZeroHit as u8);
                        }
                    } else {
                        if (self.cycle >= 1) && (self.cycle < 258) {
                            self.registers.status.set(Status::SpriteZeroHit as u8);
                        }
                    }
                }
            }
        }

        (pixel, palette)
    }

    // This little function "flips" a byte
    // so 0b11100000 becomes 0b00000111. It's very
    // clever, and stolen completely from here:
    // https://stackoverflow.com/a/2602885
    fn flipbyte(&self, mut b: u8) -> u8 {
        b = ((b & 0xF0) >> 4) | ((b & 0x0F) << 4);
        b = ((b & 0xCC) >> 2) | ((b & 0x33) << 2);
        b = ((b & 0xAA) >> 1) | ((b & 0x55) << 1);
        b
    }
}