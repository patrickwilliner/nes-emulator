pub trait Mapper {
    fn new(prg_banks: u8, chr_banks: u8) -> Self;

    fn cpu_map_read(addr: u16) -> Option<u32>;
	fn cpu_map_write(addr: u16) -> Option<u32>;
	fn ppu_map_read(addr: u16) -> Option<u32>;
	fn ppu_map_write(addr: u16) -> Option<u32>;
}

pub struct Mapper000 {
    prg_banks: u8,
	chr_banks: u8
}

impl Mapper000 {
    pub fn new(prg_banks: u8, chr_banks: u8) -> Mapper000 {
        Mapper000 {
            prg_banks,
            chr_banks
        }
    }

    pub fn cpu_map_read(&self, addr: u16) -> Option<u32> {
        // if PRGROM is 16KB
        //     CPU Address Bus          PRG ROM
        //     0x8000 -> 0xBFFF: Map    0x0000 -> 0x3FFF
        //     0xC000 -> 0xFFFF: Mirror 0x0000 -> 0x3FFF
        // if PRGROM is 32KB
        //     CPU Address Bus          PRG ROM
        //     0x8000 -> 0xFFFF: Map    0x0000 -> 0x7FFF	
        if (addr >= 0x8000) && (addr <= 0xFFFF) {
            let mask = if self.prg_banks > 1 {
                0x7FFF
            } else {
                0x3FFF
            };

            Some((addr & mask) as u32)
        } else {
            None
        }
    }

    pub fn cpu_map_write(&self, addr: u16) -> Option<u32> {
        if (addr >= 0x8000) && (addr <= 0xFFFF) {
            let mask = if self.prg_banks > 1 {
                0x7FFF
            } else {
                0x3FFF
            };

            Some((addr & mask) as u32)
        } else {
            None
        }
    }

    pub fn ppu_map_read(&self, addr: u16) -> Option<u32> {
        // There is no mapping required for PPU
        // PPU Address Bus          CHR ROM
        // 0x0000 -> 0x1FFF: Map    0x0000 -> 0x1FFF
        if (addr >= 0x0000) && (addr <= 0x1FFF) {
            Some(addr as u32)
        } else {
            None
        }
    }

    pub fn ppu_map_write(&self, addr: u16) -> Option<u32> {
        if (addr >= 0x0000) && (addr <= 0x1FFF) {
            if self.chr_banks == 0 {
                // Treat as RAM
                return Some(addr as u32);
            }
        }

        None
    }
}