pub struct Pixel {
    pub red: u8,
    pub green: u8,
    pub blue: u8,
    pub alpha: u8
}

const BLACK_PIXEL: Pixel = Pixel {red: 0x00, green: 0x00, blue: 0x00, alpha: 0xFF };

pub struct Sprite {
    pub width: usize,
    pub height: usize,
    pub pixels: Vec<u8>
}

impl Sprite {
    pub fn new(width: usize, height: usize) -> Sprite {
        Sprite { pixels: vec![0xFF; 4 * width * height], width, height }
    }

    pub fn set_pixel(&mut self, x: i16, y: i16, pixel: &Pixel) {
        if (x as usize) < self.width && (y as usize) < self.height {
            if (x as usize) < self.width && (y as usize) < self.height {
                self.pixels[(y as usize) * self.width * 4 + ((x as usize) * 4) + 0] = pixel.red;
                self.pixels[(y as usize) * self.width * 4 + ((x as usize) * 4) + 1] = pixel.green;
                self.pixels[(y as usize) * self.width * 4 + ((x as usize) * 4) + 2] = pixel.blue;
                self.pixels[(y as usize) * self.width * 4 + ((x as usize) * 4) + 3] = pixel.alpha;
            }
        }
    }
}

pub struct Bitset {
    pub value: u8
}

impl Bitset {
    pub fn get(&self, position: u8) -> u8 {
        (self.value & (1 << position)) >> position
    }

    pub fn set(&mut self, position: u8) {
        self.value |= 1 << position;
    }

    pub fn unset(&mut self, position: u8) {
        self.value &= !(1 << position);
    }

    pub fn set_bool(&mut self, position: u8, value: bool) {
        if value {
            self.set(position);
        } else {
            self.unset(position);
        }
    }
}

const COARSE_X_MASK: u16    = 0b0000_0000_0001_1111;
const COARSE_X_SHIFT: u8    = 0;
const COARSE_Y_MASK: u16    = 0b0000_0011_1110_0000;
const COARSE_Y_SHIFT: u8    = 5;
const NAMETABLE_X_MASK: u16 = 0b0000_0100_0000_0000;
const NAMETABLE_X_SHIFT: u8 = 10;
const NAMETABLE_Y_MASK: u16 = 0b0000_1000_0000_0000;
const NAMETABLE_Y_SHIFT: u8 = 11;
const FINE_Y_MASK: u16      = 0b0111_0000_0000_0000;
const FINE_Y_SHIFT: u8      = 12;

pub struct LoopyReg {
    pub value: u16
}

impl LoopyReg {
    fn get(&self, mask: u16, shift: u8) -> u16 {
        (self.value & mask) >> shift
    }

    fn set(&mut self, mask: u16, shift: u8, value: u16) {
        self.value = ((value << shift) & mask) | (self.value & !mask);
    }

    pub fn get_coarse_x(&self) -> u16 {
        self.get(COARSE_X_MASK, COARSE_X_SHIFT)
    }

    pub fn set_coarse_x(&mut self, value: u16) {
        self.set(COARSE_X_MASK, COARSE_X_SHIFT, value);
    }

    pub fn get_coarse_y(&self) -> u16 {
        self.get(COARSE_Y_MASK, COARSE_Y_SHIFT)
    }

    pub fn set_coarse_y(&mut self, value: u16) {
        self.set(COARSE_Y_MASK, COARSE_Y_SHIFT, value);
    }

    pub fn get_nametable_x(&self) -> u16 {
        self.get(NAMETABLE_X_MASK, NAMETABLE_X_SHIFT)
    }

    pub fn set_nametable_x(&mut self, value: u16) {
        self.set(NAMETABLE_X_MASK, NAMETABLE_X_SHIFT, value);
    }

    pub fn get_nametable_y(&self) -> u16 {
        self.get(NAMETABLE_Y_MASK, NAMETABLE_Y_SHIFT)
    }

    pub fn set_nametable_y(&mut self, value: u16) {
        self.set(NAMETABLE_Y_MASK, NAMETABLE_Y_SHIFT, value);
    }

    pub fn get_fine_y(&self) -> u16 {
        self.get(FINE_Y_MASK, FINE_Y_SHIFT)
    }

    pub fn set_fine_y(&mut self, value: u16) {
        self.set(FINE_Y_MASK, FINE_Y_SHIFT, value);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get() {
        assert_eq!(Bitset{value: 0b0000_0100}.get(2), 1);
        assert_eq!(Bitset{value: 0b0000_0100}.get(0), 0);
        assert_eq!(Bitset{value: 0b0000_0100}.get(1), 0);
        assert_eq!(Bitset{value: 0b0000_0100}.get(3), 0);
        assert_eq!(Bitset{value: 0b0010_0100}.get(5), 1);

        assert_eq!(LoopyReg{value: 0b0000_0011_1110_0000}.get_coarse_y(), 0x1F);
        assert_eq!(LoopyReg{value: 0b0000_0011_1111_1111}.get_coarse_y(), 0x1F);
        assert_eq!(LoopyReg{value: 0b0000_1011_1110_0000}.get_nametable_y(), 1);
        assert_eq!(LoopyReg{value: 0b0000_0011_1110_0000}.get_nametable_y(), 0);

        let mut loopy_reg = LoopyReg{value: 0x0000 };
        assert_eq!(loopy_reg.get_nametable_y(), 0);
        loopy_reg.set_nametable_y(1);
        assert_eq!(loopy_reg.get_nametable_y(), 1);
        loopy_reg.set_nametable_y(0);
        assert_eq!(loopy_reg.get_nametable_y(), 0);

        loopy_reg.value = 0b0111_0000_0000_0000;
        assert_eq!(loopy_reg.get_fine_y(), 7);
        loopy_reg.set_fine_y(0);
        assert_eq!(loopy_reg.get_fine_y(), 0);
    }
}