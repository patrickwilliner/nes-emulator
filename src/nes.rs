use crate::cartridge::Cartridge;
use crate::bus::Bus;
use crate::c6502::C6502;
use crate::c2c02::C2c02;
use std::ops::DerefMut;

pub struct Nes<'a> {
    pub cpu: Box<C6502<'a>>,
    pub bus: Box<Bus<'a>>,
    pub ppu: Box<C2c02<'a>>,
    pub cartridge: Option<Box<Cartridge>>,
    system_clock_counter: u32
}

impl <'a> Nes <'a> {
    pub fn new() -> Nes<'a> {
        let mut bus = Box::new(Bus::new());
        let mut cpu = Box::new(C6502::new(bus.deref_mut()));
        
        bus.connect_cpu(cpu.deref_mut());

        let mut nes = Nes {
            bus,
            ppu: Box::new(C2c02::new()),
            cpu,
            cartridge: None,
            system_clock_counter: 0
        };
        
        nes.bus.connect_ppu(nes.ppu.deref_mut());

        nes
    }

    pub fn clock(&mut self) {
        self.ppu.clock();

        if self.system_clock_counter % 3 == 0 {
            // TODO move this into bus
            if self.bus.dma_transfer {
			    if self.bus.dma_dummy {
				    if (self.system_clock_counter % 2) == 1 {
					    self.bus.dma_dummy = false;
				    }
			    } else {
                    if (self.system_clock_counter % 2) == 0 {
                        self.bus.dma_data = self.bus.cpu_read(((self.bus.dma_page as u16) << 8) | (self.bus.dma_addr as u16), true);
                    } else {
                        self.ppu.oam[(self.bus.dma_addr / 4) as usize].set(self.bus.dma_addr, self.bus.dma_data);
                        self.bus.dma_addr = self.bus.dma_addr.wrapping_add(1);

                        if self.bus.dma_addr == 0x00 { // u8 overflow from addition
                            self.bus.dma_transfer = false;
                            self.bus.dma_dummy = true;
                        }
                    }
			    }
            } else {
                self.cpu.clock();
            }
        }

        if self.ppu.nmi {
            self.ppu.nmi = false;
            self.cpu.nmi();
        }

        self.system_clock_counter = self.system_clock_counter.wrapping_add(1);
    }
    
    pub fn reset(&mut self) {
        self.cpu.reset();
        self.system_clock_counter = 0;
    }

    pub fn insert_cartridge(&mut self, data: Vec<u8>) {
        self.cartridge = Some(Box::new(Cartridge::new(data)));
        self.bus.connect_cartridge(self.cartridge.as_mut().unwrap());
        self.ppu.connect_cartridge(self.cartridge.as_mut().unwrap());
    }

    pub fn render_frame(&mut self) {
        while !self.ppu.frame_complete {
            self.clock();
        }
        self.ppu.frame_complete = false;
    }

    pub fn controller_input(&mut self, data: u8) {
        self.bus.controller[0] = data;
    }
}