use crate::bus::Bus;

pub enum Flags6502 {
    C = 1 << 0, // Carry Bit
    Z = 1 << 1, // Zero Bit,
    I = 1 << 2, // Disable Interrupts
    D = 1 << 3, // Decimal Mode (unused in this implementation)
    B = 1 << 4, // Break
    U = 1 << 5, // Unused
    V = 1 << 6, // Overflow
    N = 1 << 7  // Negative
 }

 #[derive(Copy, Clone)] 
struct Instruction<'a> {
    name: &'a str,
    operate: fn(cpu: &mut C6502<'a>) -> u8,
    addr_mode: fn(cpu: &mut C6502<'a>) -> u8,
    cycles: u8
}

fn create_instruction_lookup<'a>() -> [Instruction<'a>; 256] {
    [
    Instruction{name: "BRK", operate: C6502::brk, addr_mode: C6502::imm, cycles: 7 },
    Instruction{name: "ORA", operate: C6502::ora, addr_mode: C6502::izx, cycles: 6 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 8 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 3 },
    Instruction{name: "ORA", operate: C6502::ora, addr_mode: C6502::zp0, cycles: 3 },
    Instruction{name: "ASL", operate: C6502::asl, addr_mode: C6502::zp0, cycles: 5 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 5 },
    Instruction{name: "PHP", operate: C6502::php, addr_mode: C6502::imp, cycles: 3 },
    Instruction{name: "ORA", operate: C6502::ora, addr_mode: C6502::imm, cycles: 2 },
    Instruction{name: "ASL", operate: C6502::asl, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 4 },
    Instruction{name: "ORA", operate: C6502::ora, addr_mode: C6502::abs, cycles: 4 },
    Instruction{name: "ASL", operate: C6502::asl, addr_mode: C6502::abs, cycles: 6 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 6 },
    Instruction{name: "BPL", operate: C6502::bpl, addr_mode: C6502::rel, cycles: 2 },
    Instruction{name: "ORA", operate: C6502::ora, addr_mode: C6502::izy, cycles: 5 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 8 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 4 },
    Instruction{name: "ORA", operate: C6502::ora, addr_mode: C6502::zpx, cycles: 4 },
    Instruction{name: "ASL", operate: C6502::asl, addr_mode: C6502::zpx, cycles: 6 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 6 },
    Instruction{name: "CLC", operate: C6502::clc, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "ORA", operate: C6502::ora, addr_mode: C6502::aby, cycles: 4 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 7 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 4 },
    Instruction{name: "ORA", operate: C6502::ora, addr_mode: C6502::abx, cycles: 4 },
    Instruction{name: "ASL", operate: C6502::asl, addr_mode: C6502::abx, cycles: 7 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 7 },
    Instruction{name: "JSR", operate: C6502::jsr, addr_mode: C6502::abs, cycles: 6 },
    Instruction{name: "AND", operate: C6502::and, addr_mode: C6502::izx, cycles: 6 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 8 },
    Instruction{name: "BIT", operate: C6502::bit, addr_mode: C6502::zp0, cycles: 3 },
    Instruction{name: "AND", operate: C6502::and, addr_mode: C6502::zp0, cycles: 3 },
    Instruction{name: "ROL", operate: C6502::rol, addr_mode: C6502::zp0, cycles: 5 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 5 },
    Instruction{name: "PLP", operate: C6502::plp, addr_mode: C6502::imp, cycles: 4 },
    Instruction{name: "AND", operate: C6502::and, addr_mode: C6502::imm, cycles: 2 },
    Instruction{name: "ROL", operate: C6502::rol, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "BIT", operate: C6502::bit, addr_mode: C6502::abs, cycles: 4 },
    Instruction{name: "AND", operate: C6502::and, addr_mode: C6502::abs, cycles: 4 },
    Instruction{name: "ROL", operate: C6502::rol, addr_mode: C6502::abs, cycles: 6 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 6 },
    Instruction{name: "BMI", operate: C6502::bmi, addr_mode: C6502::rel, cycles: 2 },
    Instruction{name: "AND", operate: C6502::and, addr_mode: C6502::izy, cycles: 5 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 8 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 4 },
    Instruction{name: "AND", operate: C6502::and, addr_mode: C6502::zpx, cycles: 4 },
    Instruction{name: "ROL", operate: C6502::rol, addr_mode: C6502::zpx, cycles: 6 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 6 },
    Instruction{name: "SEC", operate: C6502::sec, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "AND", operate: C6502::and, addr_mode: C6502::aby, cycles: 4 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 7 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 4 },
    Instruction{name: "AND", operate: C6502::and, addr_mode: C6502::abx, cycles: 4 },
    Instruction{name: "ROL", operate: C6502::rol, addr_mode: C6502::abx, cycles: 7 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 7 },
    Instruction{name: "RTI", operate: C6502::rti, addr_mode: C6502::imp, cycles: 6 },
    Instruction{name: "EOR", operate: C6502::eor, addr_mode: C6502::izx, cycles: 6 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 8 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 3 },
    Instruction{name: "EOR", operate: C6502::eor, addr_mode: C6502::zp0, cycles: 3 },
    Instruction{name: "LSR", operate: C6502::lsr, addr_mode: C6502::zp0, cycles: 5 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 5 },
    Instruction{name: "PHA", operate: C6502::pha, addr_mode: C6502::imp, cycles: 3 },
    Instruction{name: "EOR", operate: C6502::eor, addr_mode: C6502::imm, cycles: 2 },
    Instruction{name: "LSR", operate: C6502::lsr, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "JMP", operate: C6502::jmp, addr_mode: C6502::abs, cycles: 3 },
    Instruction{name: "EOR", operate: C6502::eor, addr_mode: C6502::abs, cycles: 4 },
    Instruction{name: "LSR", operate: C6502::lsr, addr_mode: C6502::abs, cycles: 6 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 6 },
    Instruction{name: "BVC", operate: C6502::bvc, addr_mode: C6502::rel, cycles: 2 },
    Instruction{name: "EOR", operate: C6502::eor, addr_mode: C6502::izy, cycles: 5 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 8 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 4 },
    Instruction{name: "EOR", operate: C6502::eor, addr_mode: C6502::zpx, cycles: 4 },
    Instruction{name: "LSR", operate: C6502::lsr, addr_mode: C6502::zpx, cycles: 6 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 6 },
    Instruction{name: "CLI", operate: C6502::cli, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "EOR", operate: C6502::eor, addr_mode: C6502::aby, cycles: 4 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 7 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 4 },
    Instruction{name: "EOR", operate: C6502::eor, addr_mode: C6502::abx, cycles: 4 },
    Instruction{name: "LSR", operate: C6502::lsr, addr_mode: C6502::abx, cycles: 7 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 7 },
    Instruction{name: "RTS", operate: C6502::rts, addr_mode: C6502::imp, cycles: 6 },
    Instruction{name: "ADC", operate: C6502::adc, addr_mode: C6502::izx, cycles: 6 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 8 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 3 },
    Instruction{name: "ADC", operate: C6502::adc, addr_mode: C6502::zp0, cycles: 3 },
    Instruction{name: "ROR", operate: C6502::ror, addr_mode: C6502::zp0, cycles: 5 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 5 },
    Instruction{name: "PLA", operate: C6502::pla, addr_mode: C6502::imp, cycles: 4 },
    Instruction{name: "ADC", operate: C6502::adc, addr_mode: C6502::imm, cycles: 2 },
    Instruction{name: "ROR", operate: C6502::ror, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "JMP", operate: C6502::jmp, addr_mode: C6502::ind, cycles: 5 },
    Instruction{name: "ADC", operate: C6502::adc, addr_mode: C6502::abs, cycles: 4 },
    Instruction{name: "ROR", operate: C6502::ror, addr_mode: C6502::abs, cycles: 6 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 6 },
    Instruction{name: "BVS", operate: C6502::bvs, addr_mode: C6502::rel, cycles: 2 },
    Instruction{name: "ADC", operate: C6502::adc, addr_mode: C6502::izy, cycles: 5 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 8 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 4 },
    Instruction{name: "ADC", operate: C6502::adc, addr_mode: C6502::zpx, cycles: 4 },
    Instruction{name: "ROR", operate: C6502::ror, addr_mode: C6502::zpx, cycles: 6 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 6 },
    Instruction{name: "SEI", operate: C6502::sei, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "ADC", operate: C6502::adc, addr_mode: C6502::aby, cycles: 4 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 7 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 4 },
    Instruction{name: "ADC", operate: C6502::adc, addr_mode: C6502::abx, cycles: 4 },
    Instruction{name: "ROR", operate: C6502::ror, addr_mode: C6502::abx, cycles: 7 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 7 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "STA", operate: C6502::sta, addr_mode: C6502::izx, cycles: 6 },
    Instruction{name: "???", operate: C6502::nop, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 6 },
    Instruction{name: "STY", operate: C6502::sty, addr_mode: C6502::zp0, cycles: 3 },
    Instruction{name: "STA", operate: C6502::sta, addr_mode: C6502::zp0, cycles: 3 },
    Instruction{name: "STX", operate: C6502::stx, addr_mode: C6502::zp0, cycles: 3 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 3 },
    Instruction{name: "DEY", operate: C6502::dey, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "TXA", operate: C6502::txa, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "STY", operate: C6502::sty, addr_mode: C6502::abs, cycles: 4 },
    Instruction{name: "STA", operate: C6502::sta, addr_mode: C6502::abs, cycles: 4 },
    Instruction{name: "STX", operate: C6502::stx, addr_mode: C6502::abs, cycles: 4 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 4 },
    Instruction{name: "BCC", operate: C6502::bcc, addr_mode: C6502::rel, cycles: 2 },
    Instruction{name: "STA", operate: C6502::sta, addr_mode: C6502::izy, cycles: 6 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 6 },
    Instruction{name: "STY", operate: C6502::sty, addr_mode: C6502::zpx, cycles: 4 },
    Instruction{name: "STA", operate: C6502::sta, addr_mode: C6502::zpx, cycles: 4 },
    Instruction{name: "STX", operate: C6502::stx, addr_mode: C6502::zpy, cycles: 4 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 4 },
    Instruction{name: "TYA", operate: C6502::tya, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "STA", operate: C6502::sta, addr_mode: C6502::aby, cycles: 5 },
    Instruction{name: "TXS", operate: C6502::txs, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 5 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 5 },
    Instruction{name: "STA", operate: C6502::sta, addr_mode: C6502::abx, cycles: 5 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 5 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 5 },
    Instruction{name: "LDY", operate: C6502::ldy, addr_mode: C6502::imm, cycles: 2 },
    Instruction{name: "LDA", operate: C6502::lda, addr_mode: C6502::izx, cycles: 6 },
    Instruction{name: "LDX", operate: C6502::ldx, addr_mode: C6502::imm, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imm, cycles: 6 }, 
    Instruction{name: "LDY", operate: C6502::ldy, addr_mode: C6502::zp0, cycles: 3 },
    Instruction{name: "LDA", operate: C6502::lda, addr_mode: C6502::zp0, cycles: 3 },
    Instruction{name: "LDX", operate: C6502::ldx, addr_mode: C6502::zp0, cycles: 3 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 3 },
    Instruction{name: "TAY", operate: C6502::tay, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "LDA", operate: C6502::lda, addr_mode: C6502::imm, cycles: 2 },
    Instruction{name: "TAX", operate: C6502::tax, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "LDY", operate: C6502::ldy, addr_mode: C6502::abs, cycles: 4 },
    Instruction{name: "LDA", operate: C6502::lda, addr_mode: C6502::abs, cycles: 4 },
    Instruction{name: "LDX", operate: C6502::ldx, addr_mode: C6502::abs, cycles: 4 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 4 },
    Instruction{name: "BCS", operate: C6502::bcs, addr_mode: C6502::rel, cycles: 2 },
    Instruction{name: "LDA", operate: C6502::lda, addr_mode: C6502::izy, cycles: 5 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 5 },
    Instruction{name: "LDY", operate: C6502::ldy, addr_mode: C6502::zpx, cycles: 4 },
    Instruction{name: "LDA", operate: C6502::lda, addr_mode: C6502::zpx, cycles: 4 },
    Instruction{name: "LDX", operate: C6502::ldx, addr_mode: C6502::zpy, cycles: 4 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 4 },
    Instruction{name: "CLV", operate: C6502::clv, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "LDA", operate: C6502::lda, addr_mode: C6502::aby, cycles: 4 },
    Instruction{name: "TSX", operate: C6502::tsx, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 4 },
    Instruction{name: "LDY", operate: C6502::ldy, addr_mode: C6502::abx, cycles: 4 },
    Instruction{name: "LDA", operate: C6502::lda, addr_mode: C6502::abx, cycles: 4 },
    Instruction{name: "LDX", operate: C6502::ldx, addr_mode: C6502::aby, cycles: 4 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 4 },
    Instruction{name: "CPY", operate: C6502::cpy, addr_mode: C6502::imm, cycles: 2 },
    Instruction{name: "CMP", operate: C6502::cmp, addr_mode: C6502::izx, cycles: 6 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 8 },
    Instruction{name: "CPY", operate: C6502::cpy, addr_mode: C6502::zp0, cycles: 3 },
    Instruction{name: "CMP", operate: C6502::cmp, addr_mode: C6502::zp0, cycles: 3 },
    Instruction{name: "DEC", operate: C6502::dec, addr_mode: C6502::zp0, cycles: 5 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 5 },
    Instruction{name: "INY", operate: C6502::iny, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "CMP", operate: C6502::cmp, addr_mode: C6502::imm, cycles: 2 },
    Instruction{name: "DEX", operate: C6502::dex, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "CPY", operate: C6502::cpy, addr_mode: C6502::abs, cycles: 4 },
    Instruction{name: "CMP", operate: C6502::cmp, addr_mode: C6502::abs, cycles: 4 },
    Instruction{name: "DEC", operate: C6502::dec, addr_mode: C6502::abs, cycles: 6 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 6 },
    Instruction{name: "BNE", operate: C6502::bne, addr_mode: C6502::rel, cycles: 2 },
    Instruction{name: "CMP", operate: C6502::cmp, addr_mode: C6502::izy, cycles: 5 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 8 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 4 },
    Instruction{name: "CMP", operate: C6502::cmp, addr_mode: C6502::zpx, cycles: 4 },
    Instruction{name: "DEC", operate: C6502::dec, addr_mode: C6502::zpx, cycles: 6 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 6 },
    Instruction{name: "CLD", operate: C6502::cld, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "CMP", operate: C6502::cmp, addr_mode: C6502::aby, cycles: 4 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 7 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 4 },
    Instruction{name: "CMP", operate: C6502::cmp, addr_mode: C6502::abx, cycles: 4 },
    Instruction{name: "DEC", operate: C6502::dec, addr_mode: C6502::abx, cycles: 7 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 7 },
    Instruction{name: "CPX", operate: C6502::cpx, addr_mode: C6502::imm, cycles: 2 },
    Instruction{name: "SBC", operate: C6502::sbc, addr_mode: C6502::izx, cycles: 6 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 8 },
    Instruction{name: "CPX", operate: C6502::cpx, addr_mode: C6502::zp0, cycles: 3 },
    Instruction{name: "SBC", operate: C6502::sbc, addr_mode: C6502::zp0, cycles: 3 },
    Instruction{name: "INC", operate: C6502::inc, addr_mode: C6502::zp0, cycles: 5 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 5 },
    Instruction{name: "INX", operate: C6502::inx, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "SBC", operate: C6502::sbc, addr_mode: C6502::imm, cycles: 2 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::sbc, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "CPX", operate: C6502::cpx, addr_mode: C6502::abs, cycles: 4 },
    Instruction{name: "SBC", operate: C6502::sbc, addr_mode: C6502::abs, cycles: 4 },
    Instruction{name: "INC", operate: C6502::inc, addr_mode: C6502::abs, cycles: 6 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 6 },
    Instruction{name: "BEQ", operate: C6502::beq, addr_mode: C6502::rel, cycles: 2 },
    Instruction{name: "SBC", operate: C6502::sbc, addr_mode: C6502::izy, cycles: 5 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 8 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 4 },
    Instruction{name: "SBC", operate: C6502::sbc, addr_mode: C6502::zpx, cycles: 4 },
    Instruction{name: "INC", operate: C6502::inc, addr_mode: C6502::zpx, cycles: 6 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 6 },
    Instruction{name: "SED", operate: C6502::sed, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "SBC", operate: C6502::sbc, addr_mode: C6502::aby, cycles: 4 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 2 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 7 },
    Instruction{name: "NOP", operate: C6502::nop, addr_mode: C6502::imp, cycles: 4 },
    Instruction{name: "SBC", operate: C6502::sbc, addr_mode: C6502::abx, cycles: 4 },
    Instruction{name: "INC", operate: C6502::inc, addr_mode: C6502::abx, cycles: 7 },
    Instruction{name: "???", operate: C6502::xxx, addr_mode: C6502::imp, cycles: 7 }
    ]
}

pub struct C6502<'a> {
    pub acc: u8,       // Accumulator Register
    pub x: u8,         // X Register
    pub y: u8,         // Y Register
    pub stkp: u8,      // Stack Pointer (points to location on Bus)
    pub pc: u16,       // Program Counter
    pub status: u8,    // Status Register

    pub bus: *mut Bus<'a>,

    fetched: u8,   // helper for fetching data,
    pub addr_abs: u16, // helper
    addr_rel: u16,
    opcode: u8,
    cycles: u8,

    instruction_lookup: [Instruction<'a>; 256]
}

impl <'a> C6502<'a> {
    pub fn new(bus: &mut Bus<'a>) -> C6502<'a> {
        C6502{
            acc: 0x00,
            x: 0x00,
            y: 0x00,
            stkp: 0x00,
            pc: 0x0000,
            status: 0x00,
            bus,
            fetched: 0x00,
            addr_abs: 0x0000,
            addr_rel: 0x0000,
            opcode: 0x00,
            cycles: 0,
            instruction_lookup: create_instruction_lookup()
        }
    }

    pub fn read(&self, addr: u16) -> u8 {
        unsafe {
            (*self.bus).cpu_read(addr, false)
        }
    }

    pub fn write(&mut self, addr: u16, data: u8) {
        unsafe {
            (*self.bus).cpu_write(addr, data);
        }
    }

    // get status register flag
    pub fn get_flag(&self, flag: Flags6502) -> u8 {
        if (self.status & (flag as u8)) > 0 {
            1
        } else {
            0
        }
    }

    // set status register flag
    pub fn set_flag(&mut self, flag: Flags6502, value: bool) {
        if value {
		    self.status |= flag as u8;
        } else {
            self.status &= !(flag as u8);
        }
    }

    pub fn clock(&mut self) {
        // do all the processing in the first clock cycle
        
        if self.cycles == 0 {
            // println!("{0: <40}A:{1:02X} X:{2:02X} Y:{3:02X} P:{4:02X} SP:{5:02X}", &*self.disassemble_instruction(self.pc as u32).0, self.acc, self.x, self.y, self.status, self.stkp);

            self.opcode = self.read(self.pc);

            // Always set the unused status flag bit to 1
            self.set_flag(Flags6502::U, true);

            self.inc_pc();
            
            let instruction = self.instruction_lookup[self.opcode as usize];
            self.cycles = instruction.cycles;
            
            // according to spec sheet instructions might need an additional clock cycle
            let needs_additional_clock_cycle_1 = (instruction.addr_mode)(self);
            let needs_additional_clock_cycle_2 = (instruction.operate)(self);
            
            if (needs_additional_clock_cycle_1 & needs_additional_clock_cycle_2) != 0 {
                self.cycles += 1;
            }

            // Always set the unused status flag bit to 1
            self.set_flag(Flags6502::U, true);
        }

        // ellapse clock cycle
        self.cycles -= 1;
    }

    pub fn is_complete(&self) -> bool {
        self.cycles == 0
    }

    pub fn reset(&mut self) {
        // reset pc
        self.addr_abs = 0xFFFC;
        let lo: u16 = self.read(self.addr_abs.wrapping_add(0)) as u16;
        let hi: u16 = self.read(self.addr_abs.wrapping_add(1)) as u16;

        self.pc = (hi << 8) | lo;
        
        // reset registers
        self.acc = 0;
        self.x = 0;
        self.y = 0;
        self.stkp = 0xFD;
        self.status = 0x00 | (Flags6502::U as u8);

        // reset helpers
        self.addr_rel =  0x0000;
        self.addr_abs = 0x0000;
        self.fetched = 0x00;
        self.cycles = 8;
    }

    // interrupt
    // writes data to the stack
    pub fn irq(&mut self) {
        if self.get_flag(Flags6502::I) == 0 {
            // Push the program counter to the stack. It's 16-bits dont
            // forget so that takes two pushes
            self.write((self.stkp as u16).wrapping_add(0x0100), (((self.pc as u16) >> 8) & 0x00FF) as u8);
            self.dec_stkp();
            self.write((self.stkp as u16).wrapping_add(0x0100), ((self.pc as u16) & 0x00FF) as u8);
            self.dec_stkp();

            // Then Push the status register to the stack
            self.set_flag(Flags6502::B, false);
            self.set_flag(Flags6502::U, true);
            self.set_flag(Flags6502::I, true);
            self.write((self.stkp as u16).wrapping_add(0x0100), self.status);
            self.dec_stkp();

            // Read new program counter location from fixed address
            self.addr_abs = 0xFFFE;
            let lo: u16 = self.read(self.addr_abs) as u16;
            let hi: u16 = self.read(self.addr_abs.wrapping_add(1)) as u16;
            self.pc = (hi << 8) | lo;

            self.cycles = 7;
        }
    }

    // non maskable interrupt
    // i.e. same as irq but cannot be disabled by I flag
    pub fn nmi(&mut self) {
        // write current program counter to stack
        self.write((self.stkp as u16).wrapping_add(0x0100), ((self.pc >> 8) & 0x00FF) as u8);
        self.dec_stkp();
        self.write((self.stkp as u16).wrapping_add(0x0100), (self.pc & 0x00FF) as u8);
        self.dec_stkp();

        // write the status register to the stack
        self.set_flag(Flags6502::B, false);
        self.set_flag(Flags6502::U, true);
        self.set_flag(Flags6502::I, true);
        self.write((self.stkp as u16).wrapping_add(0x0100), self.status);
        self.dec_stkp();

        // load new pc from hardcoded address 0xFFFE
        self.addr_abs = 0xFFFA;
        let lo: u16 = self.read(self.addr_abs.wrapping_add(0)) as u16;
        let hi: u16 = self.read(self.addr_abs.wrapping_add(1)) as u16;
        self.pc = (hi << 8) | lo;

        self.cycles = 8;
    }

    fn fetch(&mut self) -> u8 {
        // fetch data for all instructions except when addressing mode is IMP
        if !self.address_mode_is_imp() {
            self.fetched = self.read(self.addr_abs);
        }
        self.fetched
    }
}

// Addressing Modes
impl <'a> C6502<'a> {
    // implied i.e. no data is part of the instruction; instruction might use acc
    pub fn imp(cpu: &mut C6502<'a>) -> u8 {
        cpu.fetched = cpu.acc;
        0
    }

    // immediate mode addressing i.e. memory address will be next byte
    pub fn imm(cpu: &mut C6502<'a>) -> u8 {
        cpu.addr_abs = cpu.pc;
        cpu.inc_pc();
        0
    }

    // memory is organised in pages - example 0xAA22 - AA page number; 22 page data
    // zero page addressing -> page is zero
    pub fn zp0(cpu: &mut C6502<'a>) -> u8 {
        cpu.addr_abs = cpu.read(cpu.pc) as u16;
        cpu.inc_pc();
        cpu.addr_abs &= 0x00FF;
        0
    }

    // zero page addressing with x register offset
    pub fn zpx(cpu: &mut C6502<'a>) -> u8 {
        cpu.addr_abs = cpu.read(cpu.pc).wrapping_add(cpu.x) as u16;
        cpu.inc_pc();
        cpu.addr_abs &= 0x00FF;
        0
    }

    // zero page addressing with y register offset
    pub fn zpy(cpu: &mut C6502<'a>) -> u8 {
        cpu.addr_abs = cpu.read(cpu.pc).wrapping_add(cpu.y) as u16;
        cpu.inc_pc();
        cpu.addr_abs &= 0x00FF;
        0
    }

    // relative addressing - for branch instructions
    pub fn rel(cpu: &mut C6502<'a>) -> u8 {
        cpu.addr_rel = cpu.read(cpu.pc) as u16;
        cpu.inc_pc();
        
	    if (cpu.addr_rel & 0x80) != 0 { // i.e. negative number for jumping backwards
            cpu.addr_rel |= 0xFF00; // make sure that arithmetic will work out
        }
        
        0
    }

    // full address specified
    pub fn abs(cpu: &mut C6502<'a>) -> u8 {
        let lo: u16 = cpu.read(cpu.pc) as u16;
        cpu.inc_pc();
        let hi: u16 = cpu.read(cpu.pc) as u16;
        cpu.inc_pc();
        cpu.addr_abs = (hi << 8) | lo;
        0
    }

    // abs with x register offset
    pub fn abx(cpu: &mut C6502<'a>) -> u8 {
        let lo: u16 = cpu.read(cpu.pc) as u16;
        cpu.inc_pc();
        let hi: u16 = cpu.read(cpu.pc) as u16;
        cpu.inc_pc();
        cpu.addr_abs = (hi << 8) | lo;
        cpu.addr_abs = cpu.addr_abs.wrapping_add(cpu.x as u16);
        
        // after adding the offset the whole address might change to different page
        // => indicate an additional clock cycle
        if (cpu.addr_abs & 0xFF00) != (hi << 8) {
            1
        } else {
            0
        }
    }

    // abs with y register offset
    pub fn aby(cpu: &mut C6502<'a>) -> u8 {
        let lo: u16 = cpu.read(cpu.pc) as u16;
        cpu.inc_pc();
        let hi: u16 = cpu.read(cpu.pc) as u16;
        cpu.inc_pc();
        cpu.addr_abs = (hi << 8) | lo;
        cpu.addr_abs = cpu.addr_abs.wrapping_add(cpu.y as u16);
        
        // after adding the offset the whole address might change to different page
        // => indicate an additional clock cycle
        if (cpu.addr_abs & 0xFF00) != (hi << 8) {
            1
        } else {
            0
        }
    }

    // indirect addressing -> i.e. pointers
    pub fn ind(cpu: &mut C6502<'a>) -> u8 {
        let ptr_lo: u16 = cpu.read(cpu.pc) as u16;
        cpu.inc_pc();
        let ptr_hi: u16 = cpu.read(cpu.pc) as u16;
        cpu.inc_pc();

        let ptr = (ptr_hi << 8) | ptr_lo;

        if ptr_lo == 0x00FF {
            // simulate hardware bug
            cpu.addr_abs = ((cpu.read(ptr & 0xFF00) as u16) << 8) | (cpu.read(ptr) as u16);
        } else {
            // normal behaviour
            cpu.addr_abs = ((cpu.read(ptr.wrapping_add(1)) as u16) << 8) | (cpu.read(ptr) as u16);
        }
        0
    }

    // indirect addressing of the zero page with x register offset
    pub fn izx(cpu: &mut C6502<'a>) -> u8 {
        let t: u16 = cpu.read(cpu.pc) as u16;
        cpu.inc_pc();

        let lo: u16 = cpu.read(t.wrapping_add(cpu.x as u16) & 0x00FF) as u16;
        let hi: u16 = cpu.read(t.wrapping_add(cpu.x as u16).wrapping_add(1) & 0x00FF) as u16;

        cpu.addr_abs = (hi << 8) | lo;

        0
    }

    // indirect addressing of the zero page with y register offset
    // strangly behaves differently than izx
    pub fn izy(cpu: &mut C6502<'a>) -> u8 {
        let t: u16 = cpu.read(cpu.pc) as u16;
        cpu.inc_pc();

        let lo: u16 = cpu.read(t & 0x00FF) as u16;
        let hi: u16 = cpu.read(t.wrapping_add(1) & 0x00FF) as u16;

        cpu.addr_abs = (hi << 8) | lo;
        cpu.addr_abs = cpu.addr_abs.wrapping_add(cpu.y as u16);

        if (cpu.addr_abs & 0xFF00) != (hi << 8) {
            1
        } else {
            0
        }
    }
}

// Opcodes
impl <'a> C6502<'a> {
    // Instruction: Add with Carry In
    // Function:    A = A + M + C
    // Flags Out:   C, V, N, Z
    //
    // Explanation:
    // The purpose of this function is to add a value to the accumulator and a carry bit. If
    // the result is > 255 there is an overflow setting the carry bit. Ths allows you to
    // chain together ADC instructions to add numbers larger than 8-bits. This in itself is
    // simple, however the 6502 supports the concepts of Negativity/Positivity and Signed Overflow.
    //
    // 10000100 = 128 + 4 = 132 in normal circumstances, we know this as unsigned and it allows
    // us to represent numbers between 0 and 255 (given 8 bits). The 6502 can also interpret 
    // this word as something else if we assume those 8 bits represent the range -128 to +127,
    // i.e. it has become signed.
    //
    // Since 132 > 127, it effectively wraps around, through -128, to -124. This wraparound is
    // called overflow, and this is a useful to know as it indicates that the calculation has
    // gone outside the permissable range, and therefore no longer makes numeric sense.
    //
    // Note the implementation of ADD is the same in binary, this is just about how the numbers
    // are represented, so the word 10000100 can be both -124 and 132 depending upon the 
    // context the programming is using it in. We can prove this!
    //
    //  10000100 =  132  or  -124
    // +00010001 = + 17      + 17
    //  ========    ===       ===     See, both are valid additions, but our interpretation of
    //  10010101 =  149  or  -107     the context changes the value, not the hardware!
    //
    // In principle under the -128 to 127 range:
    // 10000000 = -128, 11111111 = -1, 00000000 = 0, 00000000 = +1, 01111111 = +127
    // therefore negative numbers have the most significant set, positive numbers do not
    //
    // To assist us, the 6502 can set the overflow flag, if the result of the addition has
    // wrapped around. V <- ~(A^M) & A^(A+M+C) :D lol, let's work out why!
    //
    // Let's suppose we have A = 30, M = 10 and C = 0
    //          A = 30 = 00011110
    //          M = 10 = 00001010+
    //     RESULT = 40 = 00101000
    //
    // Here we have not gone out of range. The resulting significant bit has not changed.
    // So let's make a truth table to understand when overflow has occurred. Here I take
    // the MSB of each component, where R is RESULT.
    //
    // A  M  R | V | A^R | A^M |~(A^M) | 
    // 0  0  0 | 0 |  0  |  0  |   1   |
    // 0  0  1 | 1 |  1  |  0  |   1   |
    // 0  1  0 | 0 |  0  |  1  |   0   |
    // 0  1  1 | 0 |  1  |  1  |   0   |  so V = ~(A^M) & (A^R)
    // 1  0  0 | 0 |  1  |  1  |   0   |
    // 1  0  1 | 0 |  0  |  1  |   0   |
    // 1  1  0 | 1 |  1  |  0  |   1   |
    // 1  1  1 | 0 |  0  |  0  |   1   |
    //
    // We can see how the above equation calculates V, based on A, M and R. V was chosen
    // based on the following hypothesis:
    //       Positive Number + Positive Number = Negative Result -> Overflow
    //       Negative Number + Negative Number = Positive Result -> Overflow
    //       Positive Number + Negative Number = Either Result -> Cannot Overflow
    //       Positive Number + Positive Number = Positive Result -> OK! No Overflow
    //       Negative Number + Negative Number = Negative Result -> OK! NO Overflow
    pub fn adc(cpu: &mut C6502<'a>) -> u8 {
        cpu.fetch();
        
        let temp: u16 = (cpu.acc as u16).wrapping_add(cpu.fetched as u16).wrapping_add(cpu.get_flag(Flags6502::C) as u16);

        cpu.set_flag(Flags6502::C, temp > 255);
        cpu.set_flag(Flags6502::Z, (temp & 0x00FF) == 0);
        cpu.set_flag(Flags6502::N, (temp & 0x80) != 0);
        cpu.set_flag(Flags6502::V, ((!((cpu.acc as u16) ^ (cpu.fetched as u16)) & ((cpu.acc as u16) ^ temp)) & 0x0080) != 0);

        cpu.acc = (temp & 0x00FF) as u8;

        1
    }

    // AND operation on ACC register
    pub fn and(cpu: &mut C6502<'a>) -> u8 {
        cpu.fetch();
        cpu.acc = cpu.acc & cpu.fetched;
        cpu.set_flag(Flags6502::Z, cpu.acc == 0x00);
        cpu.set_flag(Flags6502::N, cpu.acc & 0x80 != 0);
        1
    }

    // Arithmetic Shift Left
    pub fn asl(cpu: &mut C6502<'a>) -> u8 {
        cpu.fetch();
        let temp = (cpu.fetched as u16) << 1;
        cpu.set_flag(Flags6502::C, (temp & 0xFF00) > 0);
        cpu.set_flag(Flags6502::Z, (temp & 0x00FF) == 0x00);
        cpu.set_flag(Flags6502::N, (temp & 0x80) != 0);

        if cpu.address_mode_is_imp() {
            cpu.acc = (temp & 0x00FF) as u8;
        } else {
            cpu.write(cpu.addr_abs, (temp & 0x00FF) as u8);
        }
        0
    }

    // branch if carry bit of the status register is clear
    pub fn bcc(cpu: &mut C6502<'a>) -> u8 {
        if cpu.get_flag(Flags6502::C) == 0 {
            cpu.cycles += 1;
            cpu.addr_abs = cpu.pc.wrapping_add(cpu.addr_rel);

            if (cpu.addr_abs & 0xFF00) != (cpu.pc & 0xFF00) {
                cpu.cycles += 1;
            }

            cpu.pc = cpu.addr_abs;
        }
        0
    }

    // branch if carry bit of the status register is set
    pub fn bcs(cpu: &mut C6502<'a>) -> u8 {
        if cpu.get_flag(Flags6502::C) != 0 {
            cpu.cycles += 1;
            cpu.addr_abs = cpu.pc.wrapping_add(cpu.addr_rel);

            if (cpu.addr_abs & 0xFF00) != (cpu.pc & 0xFF00) {
                cpu.cycles += 1;
            }

            cpu.pc = cpu.addr_abs;
        }
        0
    }

    // branch if equal
    pub fn beq(cpu: &mut C6502<'a>) -> u8 {
        if cpu.get_flag(Flags6502::Z) == 1 {
            cpu.cycles += 1;
            cpu.addr_abs = cpu.pc.wrapping_add(cpu.addr_rel);

            if (cpu.addr_abs & 0xFF00) != (cpu.pc & 0xFF00) {
                cpu.cycles += 1;
            }

            cpu.pc = cpu.addr_abs;
        }
        0
    }

    // BIt Test
    pub fn bit(cpu: &mut C6502<'a>) -> u8 {
        cpu.fetch();
        let temp = cpu.acc & cpu.fetched;
        cpu.set_flag(Flags6502::Z, (temp & 0x00FF) == 0x00);
        cpu.set_flag(Flags6502::N, (cpu.fetched & (1 << 7)) != 0);
        cpu.set_flag(Flags6502::V, (cpu.fetched & (1 << 6)) != 0);
        0
    }

    // branch if negative
    pub fn bmi(cpu: &mut C6502<'a>) -> u8 {
        if cpu.get_flag(Flags6502::N) == 1 {
            cpu.cycles += 1;
            cpu.addr_abs = cpu.pc.wrapping_add(cpu.addr_rel);

            if (cpu.addr_abs & 0xFF00) != (cpu.pc & 0xFF00) {
                cpu.cycles += 1;
            }

            cpu.pc = cpu.addr_abs;
        }
        0
    }

    // branch if not equal
    pub fn bne(cpu: &mut C6502<'a>) -> u8 {
        if cpu.get_flag(Flags6502::Z) == 0 {
            cpu.cycles += 1;
            cpu.addr_abs = cpu.pc.wrapping_add(cpu.addr_rel);

            if (cpu.addr_abs & 0xFF00) != (cpu.pc & 0xFF00) {
                cpu.cycles += 1;
            }

            cpu.pc = cpu.addr_abs;
        }
        0
    }

    // branch if positive
    pub fn bpl(cpu: &mut C6502<'a>) -> u8 {
        if cpu.get_flag(Flags6502::N) == 0 {
            cpu.cycles += 1;
            cpu.addr_abs = cpu.pc.wrapping_add(cpu.addr_rel);

            if (cpu.addr_abs & 0xFF00) != (cpu.pc & 0xFF00) {
                cpu.cycles += 1;
            }

            cpu.pc = cpu.addr_abs;
        }
        0
    }

    // Break
    pub fn brk(cpu: &mut C6502<'a>) -> u8 {
        cpu.inc_pc();
	
        cpu.set_flag(Flags6502::I, true);
        cpu.write((cpu.stkp as u16).wrapping_add(0x0100), ((cpu.pc >> 8) & 0x00FF) as u8);
        cpu.dec_stkp();
        cpu.write((cpu.stkp as u16).wrapping_add(0x0100), (cpu.pc & 0x00FF) as u8);
        cpu.dec_stkp();

        cpu.set_flag(Flags6502::B, true);
        cpu.write((cpu.stkp as u16).wrapping_add(0x0100), cpu.status);
        cpu.dec_stkp();
        cpu.set_flag(Flags6502::B, false);

        cpu.pc = (cpu.read(0xFFFE) as u16) | ((cpu.read(0xFFFF) as u16) << 8);
        0
    }

    // branch if overflowed
    pub fn bvc(cpu: &mut C6502<'a>) -> u8 {
        if cpu.get_flag(Flags6502::V) == 0 {
            cpu.cycles += 1;
            cpu.addr_abs = cpu.pc.wrapping_add(cpu.addr_rel);

            if (cpu.addr_abs & 0xFF00) != (cpu.pc & 0xFF00) {
                cpu.cycles += 1;
            }

            cpu.pc = cpu.addr_abs;
        }
        0
    }

    // branch if not overflowed
    pub fn bvs(cpu: &mut C6502<'a>) -> u8 {
        if cpu.get_flag(Flags6502::V) == 1 {
            cpu.cycles += 1;
            cpu.addr_abs = cpu.pc.wrapping_add(cpu.addr_rel);

            if (cpu.addr_abs & 0xFF00) != (cpu.pc & 0xFF00) {
                cpu.cycles += 1;
            }

            cpu.pc = cpu.addr_abs;
        }
        0
    }

    // clear the Carry bit
    pub fn clc(cpu: &mut C6502<'a>) -> u8 {
        cpu.set_flag(Flags6502::C, false);
        0
    }

    // clear Decimal Mode bit
    pub fn cld(cpu: &mut C6502<'a>) -> u8 {
        cpu.set_flag(Flags6502::D, false);
        0
    }

    // clear Disable Interrupts bit
    pub fn cli(cpu: &mut C6502<'a>) -> u8 {
        cpu.set_flag(Flags6502::I, false);
        0
    }

    // clear Overflow bit
    pub fn clv(cpu: &mut C6502<'a>) -> u8 {
        cpu.set_flag(Flags6502::V, false);
        0
    }

    // Compare Accumulator
    pub fn cmp(cpu: &mut C6502<'a>) -> u8 {
        cpu.fetch();
        let temp = (cpu.acc as u16).wrapping_sub(cpu.fetched as u16);
        cpu.set_flag(Flags6502::C, cpu.acc >= cpu.fetched);
        cpu.set_flag(Flags6502::Z, (temp & 0x00FF) == 0x0000);
        cpu.set_flag(Flags6502::N, (temp & 0x0080) != 0);
        1
    }

    // Compare X Register
    pub fn cpx(cpu: &mut C6502<'a>) -> u8 {
        cpu.fetch();
        let temp = (cpu.x as u16).wrapping_sub(cpu.fetched as u16);
        cpu.set_flag(Flags6502::C, cpu.x >= cpu.fetched);
        cpu.set_flag(Flags6502::Z, (temp & 0x00FF) == 0x0000);
        cpu.set_flag(Flags6502::N, (temp & 0x0080) != 0);
        0
    }

    // Compare Y Register
    pub fn cpy(cpu: &mut C6502<'a>) -> u8 {
        cpu.fetch();
        let temp = (cpu.y as u16).wrapping_sub(cpu.fetched as u16);
        cpu.set_flag(Flags6502::C, cpu.y >= cpu.fetched);
        cpu.set_flag(Flags6502::Z, (temp & 0x00FF) == 0x0000);
        cpu.set_flag(Flags6502::N, (temp & 0x0080) != 0);
        0
    }

    // Decrement Value at Memory Location
    pub fn dec(cpu: &mut C6502<'a>) -> u8 {
        cpu.fetch();
        let temp = cpu.fetched.wrapping_sub(1);
        cpu.write(cpu.addr_abs, temp & 0x00FF);
        cpu.set_flag(Flags6502::Z, (temp & 0x00FF) == 0x0000);
        cpu.set_flag(Flags6502::N, (temp & 0x0080) != 0);
        0
    }

    // Decrement X Register
    pub fn dex(cpu: &mut C6502<'a>) -> u8 {
        cpu.x = cpu.x.wrapping_sub(1);
        cpu.set_flag(Flags6502::Z, cpu.x == 0x00);
        cpu.set_flag(Flags6502::N, (cpu.x & 0x80) != 0);
        0
    }

    // Decrement Y Register
    pub fn dey(cpu: &mut C6502<'a>) -> u8 {
        cpu.y = cpu.y.wrapping_sub(1);
        cpu.set_flag(Flags6502::Z, cpu.y == 0x00);
        cpu.set_flag(Flags6502::N, (cpu.y & 0x80) != 0);
        0
    }

    // Bitwise Logic XOR
    pub fn eor(cpu: &mut C6502<'a>) -> u8 {
        cpu.fetch();
        cpu.acc = cpu.acc ^ cpu.fetched;
        cpu.set_flag(Flags6502::Z, cpu.acc == 0x00);
        cpu.set_flag(Flags6502::N, (cpu.acc & 0x80) != 0);
        1
    }

    // Increment Value at Memory Location
    pub fn inc(cpu: &mut C6502<'a>) -> u8 {
        cpu.fetch();
        let temp = cpu.fetched.wrapping_add(1);
        cpu.write(cpu.addr_abs, temp & 0x00FF);
        cpu.set_flag(Flags6502::Z, (temp & 0x00FF) == 0x0000);
        cpu.set_flag(Flags6502::N, (temp & 0x0080) != 0);
        0
    }

    // Increment X Register
    pub fn inx(cpu: &mut C6502<'a>) -> u8 {
        cpu.x = cpu.x.wrapping_add(1);
	    cpu.set_flag(Flags6502::Z, cpu.x == 0x00);
	    cpu.set_flag(Flags6502::N, (cpu.x & 0x80) != 0);
        0
    }

    // Increment Y Register
    pub fn iny(cpu: &mut C6502<'a>) -> u8 {
        cpu.y = cpu.y.wrapping_add(1);
	    cpu.set_flag(Flags6502::Z, cpu.y == 0x00);
	    cpu.set_flag(Flags6502::N, (cpu.y & 0x80) != 0);
        0
    }

    // Jump To Location
    pub fn jmp(cpu: &mut C6502<'a>) -> u8 {
        cpu.pc = cpu.addr_abs;
        0
    }

    // Jump To Sub-Routine
    // Push current pc to stack
    pub fn jsr(cpu: &mut C6502<'a>) -> u8 {
        cpu.pc = cpu.pc.wrapping_sub(1);

        cpu.write((cpu.stkp as u16).wrapping_add(0x0100), ((cpu.pc >> 8) & 0x00FF) as u8);
        cpu.stkp = cpu.stkp.wrapping_sub(1);
        cpu.write((cpu.stkp as u16).wrapping_add(0x0100), (cpu.pc & 0x00FF) as u8);
        cpu.stkp = cpu.stkp.wrapping_sub(1);

        cpu.pc = cpu.addr_abs;
        0
    }

    // Load The Accumulator
    pub fn lda(cpu: &mut C6502<'a>) -> u8 {
        cpu.fetch();
        cpu.acc = cpu.fetched;
        cpu.set_flag(Flags6502::Z, cpu.acc == 0x00);
        cpu.set_flag(Flags6502::N, (cpu.acc & 0x80) != 0);
        1
    }

    // Load The X Register
    pub fn ldx(cpu: &mut C6502<'a>) -> u8 {
        cpu.fetch();
        cpu.x = cpu.fetched;
        cpu.set_flag(Flags6502::Z, cpu.x == 0x00);
        cpu.set_flag(Flags6502::N, (cpu.x & 0x80) != 0);
        1
    }

    // Load The Y Register
    pub fn ldy(cpu: &mut C6502<'a>) -> u8 {
        cpu.fetch();
        cpu.y = cpu.fetched;
        cpu.set_flag(Flags6502::Z, cpu.y == 0x00);
        cpu.set_flag(Flags6502::N, (cpu.y & 0x80) != 0);
        1
    }

    // Logical Shift one bit Right memory or accumulator
    pub fn lsr(cpu: &mut C6502<'a>) -> u8 {
        cpu.fetch();
        cpu.set_flag(Flags6502::C, (cpu.fetched & 0x0001) != 0);
        let temp = cpu.fetched >> 1;	
        cpu.set_flag(Flags6502::Z, (temp & 0x00FF) == 0x0000);
        cpu.set_flag(Flags6502::N, (temp & 0x0080) != 0);

        if cpu.address_mode_is_imp() {
            cpu.acc = (temp & 0x00FF) as u8;
        } else {
            cpu.write(cpu.addr_abs, temp & 0x00FF);
        }

        0
    }

    // No OPeration
    pub fn nop(cpu: &mut C6502<'a>) -> u8 {
        match cpu.opcode {
            0x1C | 0x3C | 0x5C | 0x7C | 0xDC | 0xFC => 1,
            _ => 0
        }
    }

    // Bitwise Logic OR
    pub fn ora(cpu: &mut C6502<'a>) -> u8 {
        cpu.fetch();
        cpu.acc = cpu.acc | cpu.fetched;
        cpu.set_flag(Flags6502::Z, cpu.acc == 0x00);
        cpu.set_flag(Flags6502::N, (cpu.acc & 0x80) != 0);
        1
    }

    // push ACC to Stack
    // 0x0100 is the start address of the Stack
    pub fn pha(cpu: &mut C6502<'a>) -> u8 {
        cpu.write((cpu.stkp as u16).wrapping_add(0x0100), cpu.acc);
        cpu.stkp = cpu.stkp.wrapping_sub(1);
        0
    }

    // Push Status Register to Stack
    // TODO review flags
    pub fn php(cpu: &mut C6502<'a>) -> u8 {
        cpu.write((cpu.stkp as u16).wrapping_add(0x0100), cpu.status | (Flags6502::B as u8) | (Flags6502::U as u8));
	    cpu.set_flag(Flags6502::B, false);
	    cpu.set_flag(Flags6502::U, false);
	    cpu.stkp = cpu.stkp.wrapping_sub(1);
        0
    }

    // pull from Stack to ACC
    // 0x0100 is the start address of the Stack
    pub fn pla(cpu: &mut C6502<'a>) -> u8 {
        cpu.inc_stkp();
        cpu.acc = cpu.read((cpu.stkp as u16).wrapping_add(0x0100));
        cpu.set_flag(Flags6502::Z, cpu.acc == 0x00);
        cpu.set_flag(Flags6502::N, (cpu.acc & 0x80) != 0);
        0
    }

    // Pop Status Register off Stack
    // TODO review flags
    pub fn plp(cpu: &mut C6502<'a>) -> u8 {
        cpu.inc_stkp();
	    cpu.status = cpu.read((cpu.stkp as u16).wrapping_add(0x0100));
        
        cpu.set_flag(Flags6502::U, true);
        // cpu.set_flag(Flags6502::C, cpu.status & 0b00000001 > 0);
        // cpu.set_flag(Flags6502::Z, cpu.status & 0b00000010 > 0);
        // cpu.set_flag(Flags6502::I, cpu.status & 0b00000100 > 0);
        // cpu.set_flag(Flags6502::D, cpu.status & 0b00001000 > 0);
        // cpu.set_flag(Flags6502::V, cpu.status & 0b01000000 > 0);
        // cpu.set_flag(Flags6502::N, cpu.status & 0b10000000 > 0);
        0
    }

    // ROtate one bit Left memory or accumulator
    pub fn rol(cpu: &mut C6502<'a>) -> u8 {
        cpu.fetch();
        let temp: u16 = ((cpu.fetched as u16) << 1) | (cpu.get_flag(Flags6502::C) as u16);
        cpu.set_flag(Flags6502::C, (temp & 0xFF00) != 0);
        cpu.set_flag(Flags6502::Z, (temp & 0x00FF) == 0x0000);
        cpu.set_flag(Flags6502::N, (temp & 0x0080) != 0);
        
        if cpu.address_mode_is_imp() {
            cpu.acc = (temp & 0x00FF) as u8;
        } else {
            cpu.write(cpu.addr_abs, (temp & 0x00FF) as u8);
        }

        0
    }

    // ROtate one bit Right memory or accumulator
    pub fn ror(cpu: &mut C6502<'a>) -> u8 {
        cpu.fetch();
        let temp: u16 = ((cpu.get_flag(Flags6502::C) as u16) << 7) | ((cpu.fetched as u16) >> 1);
        cpu.set_flag(Flags6502::C, (cpu.fetched & 0x01) != 0);
        cpu.set_flag(Flags6502::Z, (temp & 0x00FF) == 0x00);
        cpu.set_flag(Flags6502::N, (temp & 0x0080) != 0);

        if cpu.address_mode_is_imp() {
            cpu.acc = (temp & 0x00FF) as u8;
        } else {
            cpu.write(cpu.addr_abs, (temp & 0x00FF) as u8);
        }
        0
    }

    // return from interrupt
    // i.e. restore status and pc
    // TODO review
    pub fn rti(cpu: &mut C6502<'a>) -> u8 {
        cpu.inc_stkp();
        cpu.status = cpu.read((cpu.stkp as u16).wrapping_add(0x0100));
        cpu.status &= !(Flags6502::B as u8);
        cpu.status &= !(Flags6502::U as u8);
        // cpu.set_status_register_from_byte(value: u8);

        cpu.inc_stkp();
        cpu.pc = cpu.read((cpu.stkp as u16).wrapping_add(0x0100)) as u16;
        cpu.inc_stkp();
        cpu.pc |= (cpu.read((cpu.stkp as u16).wrapping_add(0x0100)) as u16) << 8;

        0
    }

    // ReTurn from Subroutine
    pub fn rts(cpu: &mut C6502<'a>) -> u8 {
        cpu.inc_stkp();
	    cpu.pc = cpu.read((cpu.stkp as u16).wrapping_add(0x0100)) as u16;
	    cpu.inc_stkp();
	    cpu.pc |= ((cpu.read((cpu.stkp as u16).wrapping_add(0x0100))) as u16) << 8;
	    cpu.inc_pc();
        0
    }

    // Instruction: Subtraction with Borrow In
    // Function:    A = A - M - (1 - C)
    // Flags Out:   C, V, N, Z
    //
    // Explanation:
    // Given the explanation for ADC above, we can reorganise our data
    // to use the same computation for addition, for subtraction by multiplying
    // the data by -1, i.e. make it negative
    //
    // A = A - M - (1 - C)  ->  A = A + -1 * (M - (1 - C))  ->  A = A + (-M + 1 + C)
    //
    // To make a signed positive number negative, we can invert the bits and add 1
    // (OK, I lied, a little bit of 1 and 2s complement :P)
    //
    //  5 = 00000101
    // -5 = 11111010 + 00000001 = 11111011 (or 251 in our 0 to 255 range)
    //
    // The range is actually unimportant, because if I take the value 15, and add 251
    // to it, given we wrap around at 256, the result is 10, so it has effectively 
    // subtracted 5, which was the original intention. (15 + 251) % 256 = 10
    //
    // Note that the equation above used (1-C), but this got converted to + 1 + C.
    // This means we already have the +1, so all we need to do is invert the bits
    // of M, the data(!) therfore we can simply add, exactly the same way we did 
    // before.
    // TODO review flags


    // fetch();
	
	// // Operating in 16-bit domain to capture carry out
	
	// // We can invert the bottom 8 bits with bitwise xor
	// uint16_t value = ((uint16_t)fetched) ^ 0x00FF;
	
	// // Notice this is exactly the same as addition from here!
	// temp = (uint16_t)a + value + (uint16_t)GetFlag(C);
	// SetFlag(C, temp & 0xFF00);
	// SetFlag(Z, ((temp & 0x00FF) == 0));
	// SetFlag(V, (temp ^ (uint16_t)a) & (temp ^ value) & 0x0080);
	// SetFlag(N, temp & 0x0080);
	// a = temp & 0x00FF;
	// return 1;


    pub fn sbc(cpu: &mut C6502<'a>) -> u8 {
        cpu.fetch();
	
        // let value: u16 = (cpu.fetched as u16) ^ 0x00FF;
        // let temp: u16 = (cpu.acc as u16).wrapping_add(value).wrapping_add(cpu.get_flag(Flags6502::C) as u16);

        // cpu.set_flag(Flags6502::C, (temp & 0xFF00) > 0);
        // cpu.set_flag(Flags6502::Z, (temp & 0x00FF) == 0);
        // cpu.set_flag(Flags6502::V, (temp ^ (cpu.acc as u16) & (temp ^ value) & 0x0080) > 0);
        //                             ~(a ^ arg) & (a ^ sum) & 0x80;
        // cpu.set_flag(Flags6502::N, (temp & 0x0080) > 0);

        // cpu.acc = (temp & 0x00FF) as u8;
        
        // // let (x1, o1) = cpu.acc.overflowing_sub(cpu.fetched);
        // // let (x2, o2) = x1.overflowing_sub(!cpu.get_flag(Flags6502::C) as u8);
        // // cpu.set_flag(Flags6502::C, !(o1 | o2));
        // // let signed_sub = (cpu.acc as i8 as i16) - (cpu.fetched as i8 as i16) - (1 - (cpu.get_flag(Flags6502::C) as i16));
        // // cpu.acc = x2;
        // // cpu.set_flag(Flags6502::V, (signed_sub < -128) || (signed_sub > 127));
        // // cpu.update_accumulator_flags();

        let temp: u16 = (cpu.acc as u16).wrapping_add(!cpu.fetched as u16).wrapping_add(cpu.get_flag(Flags6502::C) as u16);

        cpu.set_flag(Flags6502::C, temp > 255);
        cpu.set_flag(Flags6502::Z, (temp & 0x00FF) == 0);
        cpu.set_flag(Flags6502::N, (temp & 0x80) != 0);
        cpu.set_flag(Flags6502::V, ((!((cpu.acc as u16) ^ (!cpu.fetched as u16)) & ((cpu.acc as u16) ^ temp)) & 0x0080) != 0);

        cpu.acc = (temp & 0x00FF) as u8;

        1
    }

    // Set Carry Flag
    pub fn sec(cpu: &mut C6502<'a>) -> u8 {
        cpu.set_flag(Flags6502::C, true);
        0
    }

    // Set Decimal Flag
    pub fn sed(cpu: &mut C6502<'a>) -> u8 {
        cpu.set_flag(Flags6502::D, true);
        0
    }

    // Set Interrupt Flag / Enable Interrupts
    pub fn sei(cpu: &mut C6502<'a>) -> u8 {
        cpu.set_flag(Flags6502::I, true);
        0
    }

    // Store Accumulator at Address
    pub fn sta(cpu: &mut C6502<'a>) -> u8 {
        cpu.write(cpu.addr_abs, cpu.acc);
        0
    }

    // Store X Register at Address
    pub fn stx(cpu: &mut C6502<'a>) -> u8 {
        cpu.write(cpu.addr_abs, cpu.x);
        0
    }

    // Store Y Register at Address
    pub fn sty(cpu: &mut C6502<'a>) -> u8 {
        cpu.write(cpu.addr_abs, cpu.y);
        0
    }

    // Transfer Accumulator to X Register
    pub fn tax(cpu: &mut C6502<'a>) -> u8 {
        cpu.x = cpu.acc;
        cpu.set_flag(Flags6502::Z, cpu.x == 0x00);
	    cpu.set_flag(Flags6502::N, (cpu.x & 0x80) != 0);
        0
    }

    // Transfer Accumulator to Y Register
    pub fn tay(cpu: &mut C6502<'a>) -> u8 {
        cpu.y = cpu.acc;
        cpu.set_flag(Flags6502::Z, cpu.y == 0x00);
        cpu.set_flag(Flags6502::N, (cpu.y & 0x80) != 0);
        0
    }

    // Transfer Stack Pointer to X Register
    pub fn tsx(cpu: &mut C6502<'a>) -> u8 {
        cpu.x = cpu.stkp;
        cpu.set_flag(Flags6502::Z, cpu.x == 0x00);
        cpu.set_flag(Flags6502::N, (cpu.x & 0x80) != 0);
        0
    }

    // Transfer X Register to Accumulator
    pub fn txa(cpu: &mut C6502<'a>) -> u8 {
        cpu.acc = cpu.x;
	    cpu.set_flag(Flags6502::Z, cpu.acc == 0x00);
	    cpu.set_flag(Flags6502::N, (cpu.acc & 0x80) != 0);
        0
    }

    // Transfer X Register to Stack Pointer
    pub fn txs(cpu: &mut C6502<'a>) -> u8 {
        cpu.stkp = cpu.x;
        0
    }

    // Transfer Y Register to Accumulator
    pub fn tya(cpu: &mut C6502<'a>) -> u8 {
        cpu.acc = cpu.y;
	    cpu.set_flag(Flags6502::Z, cpu.acc == 0x00);
	    cpu.set_flag(Flags6502::N, (cpu.acc & 0x80) != 0);
        0
    }

    // undefined instuction i.e. no operation
    pub fn xxx(cpu: &mut C6502<'a>) -> u8 {
        0
    }
}

// flag register helpers
impl <'a> C6502<'a> {
    fn is_negative(&self, value: u8) -> bool {
        value >= 128
    }

    fn update_result_flags(&mut self, value: u8) {
        self.set_flag(Flags6502::Z, value == 0);
        self.set_flag(Flags6502::N, self.is_negative(value));
    }

    fn update_accumulator_flags(&mut self) {
        self.update_result_flags(self.acc);
    }
}

// helpers
impl <'a> C6502<'a> {
    fn inc_stkp(&mut self) {
        self.stkp = self.stkp.wrapping_add(1);
    }

    fn dec_stkp(&mut self) {
        self.stkp = self.stkp.wrapping_sub(1);
    }

    fn inc_pc(&mut self) {
        self.pc = self.pc.wrapping_add(1);
    }

    fn dec_pc(&mut self) {
        self.pc = self.pc.wrapping_sub(1);
    }

    fn set_status_register_from_byte(&mut self, value: u8) {
        self.set_flag(Flags6502::C, (value & (Flags6502::C as u8)) > 0);
        self.set_flag(Flags6502::Z, (value & (Flags6502::Z as u8)) > 0);
        self.set_flag(Flags6502::I, (value & (Flags6502::I as u8)) > 0);
        self.set_flag(Flags6502::D, (value & (Flags6502::D as u8)) > 0);
        self.set_flag(Flags6502::V, (value & (Flags6502::V as u8)) > 0);
        self.set_flag(Flags6502::N, (value & (Flags6502::N as u8)) > 0);
    }

    fn address_mode_is_imp(&self) -> bool {
        let fn_pointer_1 = C6502::imp as usize;
        let fn_pointer_2 = self.instruction_lookup[self.opcode as usize].addr_mode as usize;
        return fn_pointer_1 == fn_pointer_2;
    }

    fn is_address_mode(&self, opcode: u8, fn_pointer_1: usize) -> bool {
        let fn_pointer_2 = self.instruction_lookup[opcode as usize].addr_mode as usize;
        return fn_pointer_1 == fn_pointer_2;
    }

    fn get_bus(&self) -> &'a Bus<'a> {
        unsafe {
            return &*self.bus;
        }
    }

    // pub fn disassemble_instruction(&self, mut addr: u32) -> (String, u32) {
    //     let mut value: u8 = 0x00;
    //     let mut lo: u8 = 0x00;
    //     let mut hi: u8 = 0x00;

    //     let mut advance: u32 = 0;

    //     let mut instruction_text = String::new();
        
    //     let opcode = self.get_bus().cpu_read(addr as u16, true);
    //     let print_addr = addr;
    //     addr += 1; advance += 1;
        
    //     let instruction_name = self.instruction_lookup[opcode as usize].name;

    //     instruction_text.push_str(&*format!("${0:X}: {1: <5} {2: <3} ", print_addr, &*opcode.to_string(), instruction_name));

    //     if self.is_address_mode(opcode, C6502::imp as usize) {
    //         instruction_text.push_str("{IMP}");
    //     } else if self.is_address_mode(opcode, C6502::imm as usize) {
    //         value = self.get_bus().cpu_read(addr as u16, true);
    //         addr += 1; advance += 1;
    //         instruction_text.push_str(format!("#${:X} {{IMM}}", value).as_str());
    //     } else if self.is_address_mode(opcode, C6502::zp0 as usize) {
    //         lo = self.get_bus().cpu_read(addr as u16, true);
    //         addr += 1; advance += 1;
    //         hi = 0x00;
    //         instruction_text.push_str(format!("${:X} {{ZP0}}", lo).as_str());
    //     } else if self.is_address_mode(opcode, C6502::zpx as usize) {
    //         lo = self.get_bus().cpu_read(addr as u16, true);
    //         addr += 1; advance += 1;
    //         hi = 0x00;
    //         instruction_text.push_str(format!("${:X} X {{ZPX}}", lo).as_str());
    //     } else if self.is_address_mode(opcode, C6502::zpy as usize) {
    //         lo = self.get_bus().cpu_read(addr as u16, true);
    //         addr += 1; advance += 1;
    //         hi = 0x00;
    //         instruction_text.push_str(format!("${:X} Y {{ZPY}}", lo).as_str());
    //     } else if self.is_address_mode(opcode, C6502::izx as usize) {
    //         lo = self.get_bus().cpu_read(addr as u16, true);
    //         addr += 1; advance += 1;
    //         hi = 0x00;
    //         instruction_text.push_str(format!("(${:X} X) {{IZX}}", lo).as_str());
    //     } else if self.is_address_mode(opcode, C6502::izy as usize) {
    //         lo = self.get_bus().cpu_read(addr as u16, true);
    //         addr += 1; advance += 1;
    //         hi = 0x00;
    //         instruction_text.push_str(format!("(${:X} Y) {{IZY}}", lo).as_str());
    //     } else if self.is_address_mode(opcode, C6502::abs as usize) {
    //         lo = self.get_bus().cpu_read(addr as u16, true);
    //         addr += 1; advance += 1;
    //         hi = self.get_bus().cpu_read(addr as u16, true);
    //         addr += 1; advance += 1;
    //         instruction_text.push_str(format!("${:X} {{ABS}}", (((hi as u16) << 8) | (lo as u16))).as_str());
    //     } else if self.is_address_mode(opcode, C6502::abx as usize) {
    //         lo = self.get_bus().cpu_read(addr as u16, true);
    //         addr += 1; advance += 1;
    //         hi = self.get_bus().cpu_read(addr as u16, true);
    //         addr += 1; advance += 1;
    //         instruction_text.push_str(format!("${:X} X {{ABX}}", (((hi as u16) << 8) | (lo as u16))).as_str());
    //     } else if self.is_address_mode(opcode, C6502::aby as usize) {
    //         lo = self.get_bus().cpu_read(addr as u16, true);
    //         addr += 1; advance += 1;
    //         hi = self.get_bus().cpu_read(addr as u16, true);
    //         addr += 1; advance += 1;
    //         instruction_text.push_str(format!("${:X} Y {{ABY}}", (((hi as u16) << 8) | (lo as u16))).as_str());
    //     } else if self.is_address_mode(opcode, C6502::ind as usize) {
    //         lo = self.get_bus().cpu_read(addr as u16, true);
    //         addr += 1; advance += 1;
    //         hi = self.get_bus().cpu_read(addr as u16, true);
    //         addr += 1; advance += 1;
    //         instruction_text.push_str(format!("(${:X} Y) {{IND}}", (((hi as u16) << 8) | (lo as u16))).as_str());
    //     } else if self.is_address_mode(opcode, C6502::rel as usize) {
    //         value = self.get_bus().cpu_read(addr as u16, true);
    //         addr += 1; advance += 1;

    //         let mut addr_rel = value as u16;
    //         if (addr_rel & 0x80) != 0 { // i.e. negative number for jumping backwards
    //             addr_rel |= 0xFF00; // make sure that arithmetic will work out
    //         }

    //         instruction_text.push_str(format!("${:X} [${:X}] {{REL}}", value, (addr as u16).wrapping_add(addr_rel)).as_str());
    //     }

    //     (instruction_text, advance)
    // }

    // pub fn disassemble(&self, memStart: u16, memEnd: u16) -> Vec<String> {
    //     let mut result: Vec<String> = Vec::new();
    //     let mut addr: u32 = memStart as u32;
    //     // let mut line_addr: u32 = 0;

    //     while addr <= memEnd as u32 {
    //         // line_addr = addr;
    //         let disassemble_value = self.disassemble_instruction(addr);
    //         result.push(disassemble_value.0);
    //         addr += disassemble_value.1;
    //     }

    //     return result;
    // }
}