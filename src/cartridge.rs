use crate::mapper::Mapper000;

#[derive(Debug)]
pub enum Mirror {
    Horizontal,
    Vertical,
    OneScreenLo,
    OneScreenHi
}
    
// iNES Format Header
#[derive(Debug)]
struct Header {
    name: [u8;4],
    prg_rom_chunks: u8,
    chr_rom_chunks: u8,
    mapper1: u8,
    mapper2: u8,
    prg_ram_size: u8,
    tv_system_1: u8,
    tv_system_2: u8,
    unused: [u8;5]
}

pub struct Cartridge {
    prg_memory: Vec<u8>,
    chr_memory: Vec<u8>,
    mapper_id: u8,
    mapper: Option<Mapper000>,
	prg_banks: u8,
    chr_banks: u8,
    image_valid: bool,
    pub mirror: Mirror
}

impl Cartridge {
    pub fn new(file_bytes: Vec<u8>) -> Cartridge {
        let header = read_header(&file_bytes);

        let mut i = 16;
        
        // skip training information
        if (header.mapper1 & 0x04) != 0 {
            i += 512;
        }

        // determine mapper id
        let mapper_id = ((header.mapper2 >> 4) << 4) | (header.mapper1 >> 4);
        let mirror = if (header.mapper1 & 0x01) != 0 {
            Mirror::Vertical
        } else {
            Mirror::Horizontal
        };

        // "Discover" File Format
        let file_type: u8 = 1;

        let mut prg_memory:Vec<u8> = Vec::new();
        let mut chr_memory:Vec<u8> = Vec::new();
        
        match file_type {
            0 => {
            },
            1 => {
                
                let prg_banks_size = ((header.prg_rom_chunks as u16) * 16384) as usize;
                prg_memory = file_bytes[i..(i + prg_banks_size)].to_vec();
                i += prg_banks_size;
                
                let mut chr_banks_size = ((header.chr_rom_chunks as u16) * 8192) as usize;
                if chr_banks_size == 0 {
                    chr_banks_size = 8192;
                }
                chr_memory = file_bytes[i..(i + chr_banks_size)].to_vec();
            },
            2 => {
            },
            _ => {
            }
        }

        // Load appropriate mapper
        let mapper: Option<Mapper000> = match mapper_id {
            0 => Some(Mapper000::new(header.prg_rom_chunks, header.chr_rom_chunks)),
            _ => None
        };

        Cartridge{
            prg_memory,
            chr_memory,
            mapper_id,
            mapper,
            prg_banks: header.prg_rom_chunks,
            chr_banks: header.chr_rom_chunks,
            image_valid: true,
            mirror
        }
    }
}

impl Cartridge {
    pub fn cpu_read(&self, addr: u16, read_only: bool) -> Option<u8> {
        self.mapper.as_ref()
            .map(|mapper| mapper.cpu_map_read(addr))
            .flatten().map(|mapped_addr| {
                return self.prg_memory[mapped_addr as usize];
            })
    }

    pub fn cpu_write(&mut self, addr: u16, data: u8) {
        match self.mapper.as_ref().map(|mapper| mapper.cpu_map_write(addr)).flatten() {
            Some(mapped_addr) => {
                self.prg_memory[mapped_addr as usize] = data;
            },
            None => {}
        }
    }

    pub fn ppu_read(&self, addr: u16, read_only: bool) -> Option<u8> {
        self.mapper.as_ref()
            .map(|mapper| mapper.ppu_map_read(addr))
            .flatten().map(|mapped_addr| {
                self.chr_memory[mapped_addr as usize]
            })
    }

    pub fn ppu_write(&mut self, addr: u16, data: u8) -> bool {
        match self.mapper.as_ref().map(|mapper| mapper.ppu_map_write(addr)).flatten() {
            Some(mapped_addr) => {
                self.chr_memory[mapped_addr as usize] = data;
                true
            },
            None => false
        }
    }
}

fn read_header(file_bytes: &Vec<u8>) -> Header {
    Header {
        name: [
            file_bytes[0],
            file_bytes[1],
            file_bytes[2],
            file_bytes[3]
        ],
        prg_rom_chunks: file_bytes[4],
        chr_rom_chunks: file_bytes[5],
        mapper1: file_bytes[6],
        mapper2: file_bytes[7],
        prg_ram_size: file_bytes[8],
        tv_system_1: file_bytes[9],
        tv_system_2: file_bytes[10],
        unused: [
            file_bytes[11],
            file_bytes[12],
            file_bytes[13],
            file_bytes[14],
            file_bytes[15]
        ]
    }
}