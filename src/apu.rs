struct Sequencer {
    sequence: u32,
    new_sequence: u32,
    timer: u16,
    reload: u16,
    output: u8
}
    
impl Sequencer {
    fn new() -> Sequencer {
        Sequencer {
            sequence: 0x00000000,
            new_sequence: 0x00000000,
            timer: 0x0000,
            reload: 0x0000,
            output: 0x00
        }
    }

    // Pass in a lambda function to manipulate the sequence as required
    // by the owner of this sequencer module
    fn clock(&mut self, enable: bool, func_manip: fn(s: u32)) -> u8 {
        if enable {
            self.timer -= 1;

            if self.timer == 0xFFFF {
                self.timer = self.reload;
                func_manip(self.sequence);
                self.output = (self.sequence & 0x00000001) as u8;
            }
        }

        self.output
    }
}

struct LengthCounter {
    counter: u8
}

impl LengthCounter {
    fn new() -> LengthCounter {
        LengthCounter {
            counter: 0x00
        }
    }

    fn clock(&mut self, enable: bool, halt: bool) -> u8 {
        if !enable {
            self.counter = 0;
        } else {
            if (self.counter > 0) && !halt {
                self.counter -= 1;
            }
        }
        self.counter
    }
}

struct Envelope {
		start: bool,
		disable: bool,
		divider_count: u16,
		volume: u16,
		output: u16,
		decay_count: u16
    }
    
impl Envelope {
    fn new() -> Envelope {
        Envelope {
            start: false,
		    disable: false,
		    divider_count: 0,
		    volume: 0,
		    output: 0,
		    decay_count: 0
        }
    }

    fn clock(&mut self, bloop: bool) {
        if !self.start {
            if self.divider_count == 0 {
                self.divider_count = self.volume;

                if self.decay_count == 0 {
                    if bloop {
                        self.decay_count = 15;
                    }
                } else {
                    self.decay_count -= 1;
                }
            } else {
                self.divider_count -= 1;
            }
        } else {
            self.start = false;
            self.decay_count = 15;
            self.divider_count = self.volume;
        }

        if self.disable {
            self.output = self.volume;
        } else {
            self.output = self.decay_count;
        }
    }
}

struct Oscpulse {
    frequency: f64,
    dutycycle: f64,
    amplitude: f64,
    pi: f64,
    harmonics: f64
}

impl Oscpulse {
    fn new() -> Oscpulse {
        Oscpulse {
            frequency: 0.0,
            dutycycle: 0.0,
            amplitude: 1.0,
            pi: 3.14159,
            harmonics: 20.0
        }
    }

    fn sample(&self, t: f64) -> f64 {
        let mut a: f64 = 0.0;
        let mut b: f64 = 0.0;
        let p = self.dutycycle * 2.0 * self.pi;

        for n in 1..(self.harmonics as u16) { // TODO review
            let c: f64 = (n as f64) * self.frequency * 2.0 * self.pi * t;
            a += -self.approxsin(c) / (n as f64);
            b += -self.approxsin(c - p * (n as f64)) / (n as f64);

            //a += -sin(c) / n;
            //b += -sin(c - p * n) / n;
        }

        (2.0 * self.amplitude / self.pi) * (a - b)
    }

    fn approxsin(&self, t: f64) -> f64 {
        let j = t * 0.15915;
        j = j - (int)j;
        return 20.785 * j * (j - 0.5) * (j - 1.0f);
    }
}

struct Sweeper {
    enabled: bool,
    down: bool,
    reload: bool,
    shift: u8,
    timer: u8,
    period: u8,
    change: u16,
    mute: bool
}

impl Sweeper {
    fn new() -> Sweeper {
        Sweeper {
            enabled: false,
            down: false,
            reload: false,
            shift: 0x00,
            timer: 0x00,
            period: 0x00,
            change: 0,
            mute: false
        }
    }

    fn track(&self, target: u16) { // TODO review target
        if self.enabled {
            self.change = target >> self.shift;
            self.mute = (target < 8) || (target > 0x7FF);
        }
    }

    fn clock(&self, target: u16, channel: u8) -> Option<u16> { //TODO review &mut
        let mut result = None;
        
        if (self.timer == 0) && self.enabled && (self.shift > 0) && !self.mute {
            if (target >= 8) && (self.change < 0x07FF) {
                if self.down {
                    result = Some(target - self.change - (channel as u16));
                } else {
                    result = Some(target + self.change);
                }
            }
        }

        //if (enabled)
        {
            if (self.timer == 0) || self.reload {
                self.timer = self.period;
                self.reload = false;
            } else {
                self.timer -= 1;
            }

            self.mute = (target < 8) || (target > 0x7FF);
        }

        result
    }
}

pub struct Apu {
    frame_clock_counter: u32,
	clock_counter:       u32,
    bUseRawMode:         bool,
    length_table:        [u8; 32],

    global_time:         f64,

	// Square Wave Pulse Channel 1
	pulse1_enable:       bool,
	pulse1_halt:         bool,
	pulse1_sample:       f64,
	pulse1_output:       f64,
	pulse1_seq:          Sequencer,
	pulse1_osc:          Oscpulse,
	pulse1_env:          Envelope,
	pulse1_lc:           LengthCounter,
	pulse1_sweep:        Sweeper,

	// Square Wave Pulse Channel 2
	pulse2_enable:       bool,
	pulse2_halt:         bool,
	pulse2_sample:       f64,
	pulse2_output:       f64,
	pulse2_seq:          Sequencer,
	pulse2_osc:          Oscpulse,
	pulse2_env:          Envelope,
	pulse2_lc:           LengthCounter,
	pulse2_sweep:        Sweeper,

	// Noise Channel
	noise_enable:        bool,
	noise_halt:          bool,
	noise_env:           Envelope,
	noise_lc:            LengthCounter,
	noise_seq:           Sequencer,
	noise_sample:        f64,
    noise_output:        f64,
    
    pulse1_visual:       u16,
	pulse2_visual:       u16,
	noise_visual:        u16,
	triangle_visual:     u16
}

impl Apu {
    pub fn new() -> Apu {
        let apu = Apu {
            frame_clock_counter: 0,
	        clock_counter:       0,
            bUseRawMode:         false,
            length_table:        [ 10,  254, 20,  2, 40,  4, 80,  6,
                                   160,   8, 60, 10, 14, 12, 26, 14,
                                   12,  16, 24, 18, 48, 20, 96, 22,
                                   192,  24, 72, 26, 16, 28, 32, 30 ],

            global_time:         0.0,

            pulse1_enable:       false,
            pulse1_halt:         false,
            pulse1_sample:       0.0,
            pulse1_output:       0.0,
            pulse1_seq:          Sequencer::new(),
            pulse1_osc:          Oscpulse::new(),
            pulse1_env:          Envelope::new(),
            pulse1_lc:           LengthCounter::new(),
            pulse1_sweep:        Sweeper::new(),

            pulse2_enable:       false,
	        pulse2_halt:         false,
	        pulse2_sample:       0.0,
	        pulse2_output:       0.0,
	        pulse2_seq:          Sequencer::new(),
	        pulse2_osc:          Oscpulse::new(),
	        pulse2_env:          Envelope::new(),
	        pulse2_lc:           LengthCounter::new(),
            pulse2_sweep:        Sweeper::new(),
            
            noise_enable:        false,
            noise_halt:          false,
            noise_env:           Envelope::new(),
            noise_lc:            LengthCounter::new(),
            noise_seq:           Sequencer::new(),
            noise_sample:        0.0,
            noise_output:        0.0,

            pulse1_visual:       0,
	        pulse2_visual:       0,
	        noise_visual:        0,
	        triangle_visual:     0
        };

        apu.noise_seq.sequence = 0xDBDB;

        apu
    }

    pub fn cpu_write(&mut self, addr: u16, data: u8) {
        match addr {
            0x4000 => {
                match (data & 0xC0) >> 6 {
                    0x00 => {
                        self.pulse1_seq.new_sequence = 0b01000000;
                        self.pulse1_osc.dutycycle = 0.125;
                    },
                    0x01 => {
                        self.pulse1_seq.new_sequence = 0b01100000;
                        self.pulse1_osc.dutycycle = 0.250;
                    },
                    0x02 => {
                        self.pulse1_seq.new_sequence = 0b01111000;
                        self.pulse1_osc.dutycycle = 0.500;
                    },
                    0x03 => {
                        self.pulse1_seq.new_sequence = 0b10011111;
                        self.pulse1_osc.dutycycle = 0.750;
                    },
                    _ => {}
                }

                self.pulse1_seq.sequence = self.pulse1_seq.new_sequence;
                self.pulse1_halt = (data & 0x20) != 0;
                self.pulse1_env.volume = (data & 0x0F) as u16;
                self.pulse1_env.disable = (data & 0x10) != 0;
            },

            0x4001 => {
                self.pulse1_sweep.enabled = (data & 0x80) != 0;
                self.pulse1_sweep.period = (data & 0x70) >> 4;
                self.pulse1_sweep.down = (data & 0x08) != 0;
                self.pulse1_sweep.shift = data & 0x07;
                self.pulse1_sweep.reload = true;
            },

            0x4002 => {
                self.pulse1_seq.reload = (self.pulse1_seq.reload & 0xFF00) | (data as u16);
            },

            0x4003 => {
                self.pulse1_seq.reload = ((data & 0x07) as u16) << 8 | (self.pulse1_seq.reload & 0x00FF);
                self.pulse1_seq.timer = self.pulse1_seq.reload;
                self.pulse1_seq.sequence = self.pulse1_seq.new_sequence;
                self.pulse1_lc.counter = self.length_table[((data & 0xF8) >> 3) as usize];
                self.pulse1_env.start = true;
            },

            0x4004 => {
                match (data & 0xC0) >> 6 {
                    0x00 => {
                        self.pulse2_seq.new_sequence = 0b01000000;
                        self.pulse2_osc.dutycycle = 0.125;
                    },
                    0x01 => {
                        self.pulse2_seq.new_sequence = 0b01100000;
                        self.pulse2_osc.dutycycle = 0.250;
                    },
                    0x02 => {
                        self.pulse2_seq.new_sequence = 0b01111000;
                        self.pulse2_osc.dutycycle = 0.500;
                    },
                    0x03 => {
                        self.pulse2_seq.new_sequence = 0b10011111;
                        self.pulse2_osc.dutycycle = 0.750;
                    },
                    _ => {}
                }

                self.pulse2_seq.sequence = self.pulse2_seq.new_sequence;
                self.pulse2_halt = (data & 0x20) != 0;
                self.pulse2_env.volume = (data & 0x0F) as u16;
                self.pulse2_env.disable = (data & 0x10) != 0;
            },

            0x4005 => {
                self.pulse2_sweep.enabled = (data & 0x80) != 0;
                self.pulse2_sweep.period = (data & 0x70) >> 4;
                self.pulse2_sweep.down = (data & 0x08) != 0;
                self.pulse2_sweep.shift = data & 0x07;
                self.pulse2_sweep.reload = true;
            },

            0x4006 => {
                self.pulse2_seq.reload = (self.pulse2_seq.reload & 0xFF00) | (data as u16);
            },

            0x4007 => {
                self.pulse2_seq.reload = ((data & 0x07) as u16) << 8 | (self.pulse2_seq.reload & 0x00FF);
                self.pulse2_seq.timer = self.pulse2_seq.reload;
                self.pulse2_seq.sequence = self.pulse2_seq.new_sequence;
                self.pulse2_lc.counter = self.length_table[((data & 0xF8) >> 3) as usize];
                self.pulse2_env.start = true;
            },

            0x4008 => {
            },

            0x400C => {
                self.noise_env.volume = (data & 0x0F) as u16;
                self.noise_env.disable = (data & 0x10) != 0;
                self.noise_halt = (data & 0x20) != 0;
            },

            0x400E => {
                match data & 0x0F {
                    0x00 => {
                        self.noise_seq.reload = 0;
                    },
                    0x01 => {
                        self.noise_seq.reload = 4;
                    },
                    0x02 => {
                        self.noise_seq.reload = 8;
                    },
                    0x03 => {
                        self.noise_seq.reload = 16;
                    },
                    0x04 => {
                        self.noise_seq.reload = 32;
                    },
                    0x05 => {
                        self.noise_seq.reload = 64;
                    },
                    0x06 => {
                        self.noise_seq.reload = 96;
                    },
                    0x07 => {
                        self.noise_seq.reload = 128;
                    },
                    0x08 => {
                        self.noise_seq.reload = 160;
                    },
                    0x09 => {
                        self.noise_seq.reload = 202;
                    },
                    0x0A => {
                        self.noise_seq.reload = 254;
                    },
                    0x0B => {
                        self.noise_seq.reload = 380;
                    },
                    0x0C => {
                        self.noise_seq.reload = 508;
                    },
                    0x0D => {
                        self.noise_seq.reload = 1016;
                    },
                    0x0E => {
                        self.noise_seq.reload = 2034;
                    },
                    0x0F => {
                        self.noise_seq.reload = 4068;
                    },
                    _ => {}
                }
            },

            0x4015 => { // APU STATUS
                self.pulse1_enable = (data & 0x01) != 0;
                self.pulse2_enable = (data & 0x02) != 0;
                self.noise_enable = (data & 0x04) != 0;
            },

            0x400F => {
                self.pulse1_env.start = true;
                self.pulse2_env.start = true;
                self.noise_env.start = true;
                self.noise_lc.counter = self.length_table[((data & 0xF8) >> 3) as usize];
            }
        }
    }

	pub fn cpu_read(addr: u16) -> u8 {
        0
    }

	fn clock() {
    }

	fn reset() {
    }

	fn get_output_sample() -> f64 {
        0.0
    }
}