export function decimalToHex(d, padding) {
  let hex = Number(d).toString(16);
  padding = padding ?? 4;

  while (hex.length < padding) {
    hex = `0${hex}`;
  }

  return hex.toUpperCase();
}