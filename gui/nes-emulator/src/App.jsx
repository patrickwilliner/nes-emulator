import React, { useEffect, useState, useCallback } from 'react';
import './App.scss';
import Screen from './screen/Screen';
import Library from './library/Library';
import { getButtonState } from './controllers/KeyboardController';
import { getGamepadButtonState } from './controllers/GamepadController';
import GamepadStatus from './controllers/GamepadStatus';

let _nes;
let screenContext;
let keyState = 0;
let intervalId;

export const useWindowEvent = (event, callback) => {
  useEffect(() => {
    window.addEventListener(event, callback);
    return () => window.removeEventListener(event, callback);
  }, [event, callback]);
};

export const useGamepadConnect = (callback) => {
  return useWindowEvent('gamepadconnected', callback);
};

export const useGamepadDisconnect = (callback) => {
  return useWindowEvent('gamepaddisconnected', callback);
};

function App() {
  const [nesState, setNesState] = useState({
    on: false,
    gamepad1: false,
    gamepad2: false
  });

  useEffect(() => {
    init();
    connectNes();
    //connectControllers(nesStateRef, setNesState);
  }, []);

  useEffect(() => {
    if (nesState.on) {
      const id = setInterval(() => {
        if (nesState.gamepad1) {
          _nes.controller_input(getGamepadButtonState());
        } else {
          _nes.controller_input(keyState);
        }
    
        _nes.render_frame();
        let screen = _nes.get_screen();
        screenContext.putImageData(new ImageData(Uint8ClampedArray.from(screen), 340, 261), 0, 0);
      }, 15);
      return () => {
        clearTimeout(id);
      };
    }
  }, [nesState]);

  useEffect(() => {
    console.log('*** new state ***', nesState);
  }, [nesState]);

  const connectCallback = useCallback(event => {
    console.log(':::', nesState.on);
    setNesState({
      ...nesState,
      gamepad1: true
    });
  }, [nesState]);

  useGamepadConnect(connectCallback);

  const disconnectCallback = useCallback(event => {
    setNesState({
      ...nesState,
      gamepad1: false
    });
  }, [nesState]);

  useGamepadDisconnect(disconnectCallback);

  return (
    <div className="content">
      <div className="sidebar">
        <div className="buttons">
          <button onClick={() => nesOn(nesState, setNesState)} disabled={nesState.on}>On</button>
          <button onClick={() => nesOff(nesState, setNesState)} disabled={!nesState.on}>Off</button>
          <button onClick={() => nesReset()} disabled={!nesState.on}>Reset</button>
        </div>

        <GamepadStatus connected={nesState.gamepad1}></GamepadStatus>
        <GamepadStatus connected={nesState.gamepad2}></GamepadStatus>

        <Library onLoadRom={onLoadRom}></Library>
      </div>

      <div className="screen">
        <Screen></Screen>
      </div>
    </div>
  );
}

function init() {
  screenContext = document.getElementById('screen').getContext('2d');
}

function connectNes() {
  import('./wasm/nes.js').then(wasmJs => {
    _nes = wasmJs;
    _nes.init_panic_hook();
  });
}

function connectControllers(nesState, setNesState) {
  document.addEventListener('keydown', event => {
    keyState = getButtonState(keyState, event.keyCode, true);
  });
  document.addEventListener('keyup', event => {
    keyState = getButtonState(keyState, event.keyCode, false);
  });
}

function nesOn(nesState, setNesState) {
  setNesState({
    ...nesState,
    on: true
  });

  // createOscillator();

  _nes.reset();
}

function nesOff(nesState, setNesState) {
  clearTimeout(intervalId);
  setNesState({
    ...nesState,
    on: false
  });
  _nes.reset();
}

function nesReset() {
  _nes.reset();
}

function onLoadRom(romArrayBuffer) {
  let romData = new Uint8Array(romArrayBuffer);
  _nes.load_rom(romData);
}

function createOscillator() {
  var audioCtx = new (window.AudioContext || window.webkitAudioContext)();

  // create Oscillator node
  var oscillator = audioCtx.createOscillator();

  oscillator.type = 'square';
  oscillator.frequency.setValueAtTime(3000, audioCtx.currentTime); // value in hertz
  oscillator.connect(audioCtx.destination);
  // oscillator.start();
}

export default App;