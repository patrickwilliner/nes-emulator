import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import './Library.scss';
import FileLoader from '../file-loader/FileLoader';

const DB_VERSION = 1;
const DB_NAME = 'nes';
const DB_TABLE_ROMS = 'roms';

function Library(props) {
  const [libraryState, setLibraryState] = useState({
    roms: []
  });

  useEffect(() => {
    loadLibraryToState(libraryState, setLibraryState);
  }, []);

  return (
    <div>
      <FileLoader onLoad={createOnLoadRomCb(props.onLoadRom, libraryState, setLibraryState)}></FileLoader>
      <ul>
        {renderRoms(libraryState.roms, libraryState, setLibraryState, props.onLoadRom)}
      </ul>
    </div>
  );
}

function renderRoms(roms, libraryState, setLibraryState, onLoadRomCb) {
  let result = [];

  for (let rom of roms) {
    result.push(
      <li key={rom.id}>
        <input value={rom.name} onChange={event => onChangeRomName(event, rom, libraryState, setLibraryState)}></input>
        <button onClick={() => onClickPlay(rom, onLoadRomCb)}>Play!</button>
      </li>
    );
  }

  return result;
}

function onClickPlay(rom, onLoadRomCb) {
  onLoadRomCb(rom.data);
}

function onChangeRomName(event, rom, libraryState, setLibraryState) {
  rom.name = event.target.value;
  dbUpdateRom(rom);
  addRomToState(libraryState, setLibraryState, rom);
}

function createOnLoadRomCb(onLoadRomCb, libraryState, setLibraryState) {
  return arrayBuffer => {
    let rom = {
      name: 'New Rom',
      data: arrayBuffer
    };

    storeRom(rom).then(storedRom => {
      addRomToState(libraryState, setLibraryState, storedRom);
    });
    onLoadRomCb(arrayBuffer);
  };
}

function addRomToState(libraryState, setLibraryState, rom) {
  let roms = [];
  let isNewRom = true;

  for (let r of libraryState.roms) {
    if (r.id === rom.id) {
      roms.push(rom);
      isNewRom = false;
    } else {
      roms.push(r);
    }
  }

  if (isNewRom) {
    roms.push(rom);
  }

  setLibraryState({
    ...libraryState,
    roms: roms
  });
}

function dbUpdateRom(rom) {
  dbOpen().then(db => {
    let transaction = db.transaction([DB_TABLE_ROMS], 'readwrite');
    let store = transaction.objectStore(DB_TABLE_ROMS);
    let updatedRom = {...rom};
    store.put(updatedRom, rom.id);
  });
}

function storeRom(rom) {
  return new Promise((success, reject) => {
    dbOpen().then(db => {
      let transaction = db.transaction([DB_TABLE_ROMS], 'readwrite');
      let store = transaction.objectStore(DB_TABLE_ROMS);
      
      let request = store.add(rom);
      request.onerror = error => {
        reject(error);
      };
      request.onsuccess = event => {
        let id = event.target.result;
        success({
          ...rom,
          id: id
        });
      };
    });
  });
}

function loadLibraryToState(libraryState, setLibraryState) {
  dbLoadLibrary().then(roms => {
    setLibraryState({
      ...libraryState,
      roms: roms
    });
  });
}

function dbLoadLibrary() {
  return new Promise((success, reject) => {
    dbOpen().then(db => {
      let transaction = db.transaction([DB_TABLE_ROMS], 'readonly');
      let store = transaction.objectStore(DB_TABLE_ROMS);

      let result = [];
      let cursor = store.openCursor();

      cursor.onsuccess = event => {
        var cursor = event.target.result;
        if (cursor != null) {
          let rom = cursor.value;
          rom.id = cursor.key;
          result.push(rom);
          cursor.continue();
        } else {
          success(result);
        }
      };

      cursor.onerror = error => {
        reject(error);
      };
    });
  });
}

function dbOpen() {
  return new Promise((success, reject) => {
    let openRequest = indexedDB.open(DB_NAME, DB_VERSION);

    openRequest.onupgradeneeded = event => {
      let db = event.target.result;
      dbUpgrade(db);
    };

    openRequest.onsuccess = event => {
      let db = event.target.result;
      success(db);
    };

    openRequest.onerror = error => {
      reject(error);
    };
  });
}

function dbUpgrade(db) {
  if (!db.objectStoreNames.contains(DB_TABLE_ROMS)) {
    db.createObjectStore(DB_TABLE_ROMS, {autoIncrement: true});
  }
}

Library.propTypes = {
  onLoadRom: PropTypes.func.isRequired
};

export default Library;