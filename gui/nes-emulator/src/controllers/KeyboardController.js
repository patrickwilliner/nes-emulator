import { NesButton } from './NesButtons';

const KEY_X = 88;
const KEY_Z = 90;
const KEY_S = 83;
const KEY_A = 65;
const KEY_ARROW_UP = 38;
const KEY_ARROW_DOWN = 40;
const KEY_ARROW_LEFT = 37;
const KEY_ARROW_RIGHT = 39;

function getButtonState(keyState, keyCode, pressed) {
  switch (keyCode) {
    case KEY_X:
      pressed ? keyState |= NesButton.A : keyState &= !NesButton.A;
      break;
    case KEY_Z:
      pressed ? keyState |= NesButton.B : keyState &= !NesButton.B;
      break;
    case KEY_S:
      pressed ? keyState |= NesButton.START : keyState &= !NesButton.START;
      break;
    case KEY_A:
      pressed ? keyState |= NesButton.SELECT : keyState &= !NesButton.SELECT;
      break;
    case KEY_ARROW_UP:
      pressed ? keyState |= NesButton.UP : keyState &= !NesButton.UP;
      break;
    case KEY_ARROW_DOWN:
      pressed ? keyState |= NesButton.DOWN : keyState &= !NesButton.DOWN;
      break;
    case KEY_ARROW_LEFT:
      pressed ? keyState |= NesButton.LEFT : keyState &= !NesButton.LEFT;
      break;
    case KEY_ARROW_RIGHT:
      pressed ? keyState |= NesButton.RIGHT : keyState &= !NesButton.RIGHT;
      break;
  }
  return keyState;
}

export { getButtonState };