import { NesButton } from './NesButtons';

const PAD_A = 1;
const PAD_B = 0;
const PAD_SELECT = 8;
const PAD_START = 9;
const PAD_UP = 12;
const PAD_DOWN = 13;
const PAD_LEFT = 14;
const PAD_RIGHT = 15;

function getGamepadButtonState() {
  let buttonState = 0;

  if (navigator.getGamepads()[0].buttons[PAD_A].pressed) {
    buttonState |= NesButton.A;
  }
  if (navigator.getGamepads()[0].buttons[PAD_B].pressed) {
    buttonState |= NesButton.B;
  }
  if (navigator.getGamepads()[0].buttons[PAD_SELECT].pressed) {
    buttonState |= NesButton.SELECT;
  }
  if (navigator.getGamepads()[0].buttons[PAD_START].pressed) {
    buttonState |= NesButton.START;
  }
  if (navigator.getGamepads()[0].buttons[PAD_LEFT].pressed) {
    buttonState |= NesButton.LEFT;
  }
  if (navigator.getGamepads()[0].buttons[PAD_UP].pressed) {
    buttonState |= NesButton.UP;
  }
  if (navigator.getGamepads()[0].buttons[PAD_RIGHT].pressed) {
    buttonState |= NesButton.RIGHT;
  }
  if (navigator.getGamepads()[0].buttons[PAD_DOWN].pressed) {
    buttonState |= NesButton.DOWN;
  }

  return buttonState;
}

export { getGamepadButtonState };