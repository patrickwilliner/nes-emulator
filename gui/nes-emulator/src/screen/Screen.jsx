import React from 'react';
import './Screen.scss';

const PIXEL_SIZE = 2;

function Screen() {
  return (
    <div>
      <div>
        <canvas id="screen" width={256 * PIXEL_SIZE} height={240 * PIXEL_SIZE}></canvas>
      </div>
    </div>
  );
}

export default Screen;