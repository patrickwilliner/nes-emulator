import React from 'react';
import PropTypes from 'prop-types';
import './FileLoader.scss';

function FileLoader(props) {
  return (
    <div className="file-loader">
      <button onClick={load}>Load</button>
      <input id="file-input" className="file-input" type="file" accept=".nes" onChange={createLoadCb(props.onLoad)} />
    </div>
  );
}

function load() {
  document.getElementById('file-input').click();
}

function createLoadCb(saveRomCb) {
  return (event) => {
    let files = event.target.files;

    if (files != null && files[0] != null) {
      files[0].arrayBuffer().then(arrayBuffer => {
        saveRomCb(arrayBuffer);
      });
    }
  };
}

FileLoader.propTypes = {
  onLoad: PropTypes.func.isRequired
};

export default FileLoader;